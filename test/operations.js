(()=>{

    const arguments = require( 'command-line-args' );
    const commands = require( 'command-line-commands' );
    const usages = require( 'command-line-usage' );


    /**@constructor*/
    class Option {
        name;
        alias;
        type;
        multiple;
        value;
        required;
        description;
        typeLabel;

        constructor( { name, alias, type, multiple, value, required, description, typeLabel } ) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.multiple = multiple;
            this.value = value;
            this.required = required;
            this.description = description;
            this.typeLabel = typeLabel;
        }
    }

    /**@constructor*/
    class Section {
        name;
        descriptions;
        summary;

        /** @type {Option[]}*/
        #_options = [];

        constructor(name, descriptions, summary) {
            this.name = name;
            this.descriptions = descriptions;
            this.summary = summary;
        }

        /**
         * @param arg { Option }
         */
        set option( arg ){
            if( !arg ) return false;
            if( !arg instanceof Option ) arg = new Option( arg );
            this.#_options.push( arg );
        }

        get section(){
            const section = {};
            section.header = this.name;
            section.content = (this.descriptions)? this.descriptions : this.summary;
        }
    }



    /**@constructor*/
    class Operations {

    }

    module.exports = { Operations, Option, Section };
})();


const {Arguments} = require( '../lib/arguments.js');

const args = new Arguments( true );
args.argument = { name: "teste", type: String, file: true,  };

console.log( args.values );
