const path = require("path");
const dir = path.join( __dirname, "../");
console.log( dir );
const { FileUtil } = require( '../lib/file-util' );

FileUtil.loadAllFiles( dir, new RegExp( ".*.js$"), path => {
    console.log( path )
}, { recursive: true })