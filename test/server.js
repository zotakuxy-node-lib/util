(()=>{

    const { ServerAppBuilder, acceptAny } = require( '../lib/server-app-builder' );
    const build = new ServerAppBuilder( 3000 );
    build.withBodyParser({ json:{ } });
    build.withCors();

    build.debug = true;

    const  { server, app } = build.build();
    module.exports = { server, app };

})();