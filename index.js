const { StorageManager } = require( './lib/storage-manager' )

const { ObjectUtil } = require( "./lib/object-util");
const { ArrayUtil } = require( './lib/array-util' );
const { Arguments } = require( './lib/arguments' );
const { FileUtil, Path } = require( './lib/file-util' );
const { ProxyUtil } = require( './lib/proxy-util' );
const { ProcessTester } = require( './lib/process-tester' );
const ModuleLoader = require( './lib/module.loader' );
const { ServerAppBuilder } = require( './lib/server-app-builder' );
const { PageResolve, ContentView, isContentView } = require( './lib/page-resolve' );
module.exports = { ObjectUtil, ArrayUtil, Arguments, FileUtil, Path, ProxyUtil, ProcessTester, ModuleLoader, ServerAppBuilder, StorageManager, StaticRoute: { PageResolve, ContentView, isContentView } };
