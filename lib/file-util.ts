import fs from 'fs';
import fsPath from 'path';

export const osSlash = {
    aix: "/",
    darwin: "/",
    freebsd: "/",
    linux: "/",
    openbsd: "/",
    sunos: "/",
    win32: "\\"
}

export class Path {
    base; sub; file; dir; relative; path;

    static get regexpDelimiters() { return /[\\$]|[\/$]/ }
    static get osSep() { return fsPath.sep };

    constructor( path ) {
        Object.keys( path ).forEach( key =>{
            this[ key ] = path[ key ];
        });
    }

    get url() { return new URL( `file:${this.path}` ); }
}

export type LoadAllFileOptions={ recursive?:boolean };
export type LoadAllFileCallback = ( path:Path )=>void;

export type LoadAllFileFilter = RegExp|((_path:Path )=>boolean) ;
const _loadAllFile = function (dir:string, filter:LoadAllFileFilter, callback?:LoadAllFileCallback, base?:string, args?:LoadAllFileOptions ):Path[]|null {
    const { recursive } = ( args || { recursive:false } );

    if (!fs.existsSync( dir )){
        return null;
    }

    let sub;
    dir =  fsPath.resolve( dir+ Path.osSep ) + Path.osSep;
    if( !base ) base = dir;

    const accepts:Path[] = [];
    const files = fs.readdirSync( dir );
    for( let i=0; i<files.length; i++){
        const file = files[ i ];
        const path = fsPath.join( dir, files[i] );
        const stat = fs.lstatSync( path );
        sub = path.substr( base.length, path.length - base.length - file.length );
        const relative = `${sub}${ file }`;

        const _path = new Path({ base, sub, file, dir, relative, path } );
        if ( recursive && stat.isDirectory() ){
            const _paths = _loadAllFile( path, filter, callback, base, { recursive } );
            if( _paths ) accepts.push( ... _paths  );
        } else {
            const push = function () {

                if( callback && typeof callback === "function" ) callback( _path );
                accepts.push( _path );
            }
            if ( filter instanceof RegExp && filter.test( file ) )  push();
            else if ( typeof filter === "function" && filter( _path ) ) push();
        }
    }
    return accepts;
}

export class FileUtil {

    static loadAllFiles ( dir:string, filter:LoadAllFileFilter, callback?:LoadAllFileCallback, args?:LoadAllFileOptions ) {
        const { recursive } = ( args|| { } );
        return _loadAllFile( dir, filter, callback, undefined, { recursive } );
    }

    static scanFiles = FileUtil.loadAllFiles
}
