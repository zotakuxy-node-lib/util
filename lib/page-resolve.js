"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageResolve = exports.isContentView = void 0;
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var parseurl_1 = __importDefault(require("parseurl"));
var resolve_path_1 = __importDefault(require("resolve-path"));
function isContentView(object) {
    var _interface = object;
    return (_interface === null || _interface === void 0 ? void 0 : _interface.load) !== undefined && typeof (_interface === null || _interface === void 0 ? void 0 : _interface.load) === "function";
}
exports.isContentView = isContentView;
var PageType;
(function (PageType) {
    PageType["dynamicPage"] = ".ejs";
    PageType["staticPage"] = ".html";
})(PageType || (PageType = {}));
function PageResolve(opts) {
    var _a, _b, _c;
    var resolvers = [];
    var acceptors = [];
    var folders = { public: (_a = opts === null || opts === void 0 ? void 0 : opts.folders) === null || _a === void 0 ? void 0 : _a.public, views: (_b = opts === null || opts === void 0 ? void 0 : opts.folders) === null || _b === void 0 ? void 0 : _b.views, contents: (_c = opts === null || opts === void 0 ? void 0 : opts.folders) === null || _c === void 0 ? void 0 : _c.contents };
    var version = opts === null || opts === void 0 ? void 0 : opts.version;
    var dirSlash = opts.dirSlash;
    var hiddenIndex = opts.hiddenIndex;
    if (!Array.isArray(folders.public))
        folders.public = [folders.public];
    if (!Array.isArray(folders.views))
        folders.views = [folders.views];
    var foldersScan = [];
    ([
        //Find html in public dirs
        { folders: folders.public, callback: function (folder) { return foldersScan.push({ folder: folder, type: ".html", resolver: "static", with: "" }); } },
        //Find views in dynamics dirs
        { folders: folders.views, callback: function (folder) { return foldersScan.push({ folder: folder, type: ".ejs", resolver: "dynamic", with: "" }); } },
        //Find other file in public dirs
        { folders: folders.public, callback: function (folder) { return foldersScan.push({ folder: folder, type: "", resolver: "static", with: "" }); } },
        //Find index/html in public dirs
        { folders: folders.public, callback: function (folder) { return foldersScan.push({ folder: folder, type: ".html", resolver: "static", with: "index" }); } },
        //Find index/ejs in views dirs
        { folders: folders.views, callback: function (folder) { return foldersScan.push({ folder: folder, type: ".ejs", resolver: "dynamic", with: "index" }); } },
    ]).forEach(function (check) {
        if (check.folders)
            check.folders.forEach(function (folder) { return check.callback(folder); });
    });
    var resolveRequest = function (req) {
        // get the pathname from the URL (decoded )
        var parsed = parseurl_1.default(req);
        var pathName = !!(parsed === null || parsed === void 0 ? void 0 : parsed.pathname) ? decodeURIComponent(parsed === null || parsed === void 0 ? void 0 : parsed.pathname) : null;
        if (!pathName)
            return {
                code: 400,
                message: "path required",
                status: false,
                canNext: false,
                mapper: null,
                resolved: null,
                description: null,
                parsed: parsed
            };
        // remove leading slash
        var relative = pathName.substr(1);
        var lastResolved = {};
        var resolve = foldersScan.find(function (mapper) {
            var fullPath = resolve_path_1.default(mapper.folder, relative);
            var withFullPath;
            if (mapper.with === "index") {
                withFullPath = path_1.default.join(fullPath, "" + mapper.with + mapper.type);
            }
            else {
                withFullPath = "" + fullPath + mapper.type;
            }
            var exists = fs_1.default.existsSync(withFullPath);
            if (!exists)
                return false;
            var stat = fs_1.default.statSync(withFullPath);
            if (stat.isFile())
                return Object.assign(lastResolved, { fullPath: withFullPath, folder: mapper.folder });
        });
        var extension = path_1.default.extname(relative);
        var filename = path_1.default.basename(relative);
        var name = path_1.default.basename(filename, extension);
        var fullDir = !!(resolve) ? resolve.folder : undefined;
        var fullPath = !!(resolve) ? path_1.default.join(resolve.folder, relative) : undefined;
        var relativeDir = path_1.default.dirname(relative);
        var description = { extension: extension, filename: filename, name: name, fullPath: fullPath, fullDir: fullDir, relative: relative, relativeDir: relativeDir, folder: (resolve) ? resolve.folder : undefined,
            resolve: (resolve) ? lastResolved.fullPath : undefined,
            url: req.originalUrl,
            withRelative: (resolve) ? path_1.default.join(relative, resolve.with) : relative, needSlash: dirSlash
                && (relative.charAt(relative.length - 1) !== "/") //no has slash
                && (relative || "").length > 0 //not at the root
                && (resolve === null || resolve === void 0 ? void 0 : resolve.with) === "index" //need concat with index
        };
        return {
            code: !resolve ? 404 : 200,
            message: !resolve ? "404 request file not found" : "file found",
            status: !!resolve,
            canNext: true,
            resolved: !!resolve ? lastResolved.fullPath : null,
            mapper: resolve,
            description: description,
            parsed: parsed
        };
    };
    function listen(req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var method, result, canNext, status, message, resolved, code, mapper, description, data_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        method = req.method.toUpperCase();
                        if (method !== "GET")
                            return [2 /*return*/, next()];
                        result = resolveRequest(req);
                        canNext = result.canNext, status = result.status, message = result.message, resolved = result.resolved, code = result.code, mapper = result.mapper, description = result.description;
                        if (!status && !canNext)
                            return [2 /*return*/, reject(res, { code: code, status: status, message: message })];
                        else if (!status && canNext)
                            return [2 /*return*/, next()];
                        //Redirect to page without extension with slash/
                        if (description && description.needSlash) {
                            return [2 /*return*/, canAccept({ pageType: PageType.staticPage, description: description, data: null }, req, res, function () { return res.redirect("/" + description.relative + "/"); })];
                        }
                        //Hidden index
                        if (hiddenIndex && description && mapper && description.name === "index" && [".html", ".ejs"].includes(mapper.type)) {
                            return [2 /*return*/, canAccept({ pageType: PageType.staticPage, description: description, data: null }, req, res, function () { return res.redirect("/" + description.relativeDir + (dirSlash ? "/" : "")); })];
                        }
                        //Redirect to page without extension
                        if ((mapper === null || mapper === void 0 ? void 0 : mapper.resolver) === "static" && (description === null || description === void 0 ? void 0 : description.extension) === ".html" && description) {
                            return [2 /*return*/, canAccept({ pageType: PageType.staticPage, description: description, data: null }, req, res, function () { return res.redirect("/" + path_1.default.join(description.relativeDir, description.name)); })];
                        }
                        //Send html file if exists
                        if (description && (mapper === null || mapper === void 0 ? void 0 : mapper.resolver) === "static") {
                            return [2 /*return*/, canAccept({ pageType: PageType.staticPage, description: description, data: null }, req, res, function () { return res.sendFile(resolved); })];
                        }
                        if (!((mapper === null || mapper === void 0 ? void 0 : mapper.resolver) === "dynamic" && !!description)) return [3 /*break*/, 5];
                        if (folders.contents && fs_1.default.existsSync(path_1.default.join(folders.contents, description.withRelative + ".json")))
                            data_1 = require(path_1.default.join(folders.contents, description.relative + ".json"));
                        else if (folders.contents && fs_1.default.existsSync(path_1.default.join(folders.contents, description.withRelative + ".json5")))
                            data_1 = require(path_1.default.join(folders.contents, description.relative + ".json5"));
                        else if (folders.contents && fs_1.default.existsSync(path_1.default.join(folders.contents, description.withRelative + ".js")))
                            data_1 = require(path_1.default.join(folders.contents, description.relative + ".js"));
                        if (!isContentView(data_1)) return [3 /*break*/, 2];
                        return [4 /*yield*/, data_1.load(req, res, next)];
                    case 1:
                        data_1 = _a.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        if (!((data_1 === null || data_1 === void 0 ? void 0 : data_1.default) && isContentView(data_1 === null || data_1 === void 0 ? void 0 : data_1.default))) return [3 /*break*/, 4];
                        return [4 /*yield*/, (data_1 === null || data_1 === void 0 ? void 0 : data_1.default.load(req, res, next))];
                    case 3:
                        data_1 = _a.sent();
                        _a.label = 4;
                    case 4:
                        if (!data_1)
                            data_1 = {};
                        data_1.project = { version: version };
                        return [2 /*return*/, canAccept({ pageType: PageType.dynamicPage, description: description, data: data_1 }, req, res, function () { return res.render(description.withRelative, data_1); })];
                    case 5:
                        //Stop if resolved by special static route
                        if (resolvers.find(function (resolver) { return resolver(description, req, res); })) {
                            return [2 /*return*/, true];
                        }
                        //Next
                        return [2 /*return*/, next()];
                }
            });
        });
    }
    var canAccept = function (args, req, res, onAccept) {
        var accept = acceptors.find(function (acceptor) { return acceptor(args, req, res); });
        if (accept && onAccept)
            onAccept();
        if (accept)
            return true;
        res.status(403);
        res.json({ status: false, message: "Forbidden access to request page", code: 403 });
        return false;
    };
    function reject(res, args) {
        res.status(args.code);
        res.json(args);
        return;
    }
    return { acceptors: acceptors, resolvers: resolvers, listen: listen };
}
exports.PageResolve = PageResolve;
//# sourceMappingURL=page-resolve.js.map