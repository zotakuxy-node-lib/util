"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerAppBuilder = exports.acceptAny = void 0;
var express_1 = __importDefault(require("express"));
var ProxyUtil = require('./proxy-util').ProxyUtil;
var acceptAny = function (res, req, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', req.method);
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
};
exports.acceptAny = acceptAny;
var ServerAppBuilder = /** @class */ (function () {
    function ServerAppBuilder(port, protocol) {
        if (protocol === void 0) { protocol = "http"; }
        // /**@type { { function}[] }*/
        this._statics = [];
        this._prepare = ProxyUtil.infinityProxyObject();
        this._options = {};
        this._debug = false;
        this._port = port;
        this._protocolName = protocol;
        this._prepare.on = ProxyUtil.infinityProxyArray();
    }
    Object.defineProperty(ServerAppBuilder.prototype, "app", {
        get: function () { return this._app; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "server", {
        get: function () { return this._server; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "port", {
        get: function () { return this._port; },
        set: function (port) { this._port = port; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "protocolName", {
        get: function () { return this._protocolName; },
        set: function (protocol) { this._protocolName = protocol; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "options", {
        get: function () { return this._options; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "application", {
        get: function () { return this._application; },
        set: function (application) { this._application = application; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "debug", {
        get: function () { return this._debug; },
        set: function (debug) { this._debug = debug; },
        enumerable: false,
        configurable: true
    });
    ServerAppBuilder.prototype.statics = function () {
        var _a;
        var statics = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            statics[_i] = arguments[_i];
        }
        (_a = this._statics).push.apply(_a, statics);
        return this;
    };
    ServerAppBuilder.prototype.withCors = function (opts) {
        this._prepare.with["cors"] = function (args) {
            var app = (args || {}).app;
            var cors = require('cors');
            app.use(cors(opts));
        };
    };
    ServerAppBuilder.prototype.withBodyParser = function (opts) {
        var _this = this;
        opts = ProxyUtil.infinityProxyObject(opts);
        this._prepare.with["body-parser"] = function (_a) {
            var app = _a.app;
            console.log("with body-parser");
            var bodyParser = require('body-parser');
            Object.keys(opts).forEach(function (key) {
                if (!ProxyUtil.isInfinity(opts[key])) {
                    if (_this.debug)
                        console.log("app.use", "body-parser", key);
                    app.use(bodyParser[key](opts[key]));
                }
            });
            if (!ProxyUtil.isInfinity(opts.json))
                app.use(bodyParser.json(opts.json));
            if (!ProxyUtil.isInfinity(opts.urlencoded))
                app.use(bodyParser.urlencoded(opts.json));
            if (!ProxyUtil.isInfinity(opts.raw))
                app.use(bodyParser.raw(opts.raw));
            if (!ProxyUtil.isInfinity(opts.text))
                app.use(bodyParser.text(opts.text));
        };
    };
    ServerAppBuilder.prototype.withSession = function (args) {
        var _this = this;
        var _a = (args || {}), type = _a.type, name = _a.name, secret = _a.secret, resave = _a.resave, saveUninitialized = _a.saveUninitialized, storeConfig = _a.storeConfig, cookie = _a.cookie;
        this._prepare.with["session"] = function (_a) {
            var app = _a.app;
            var session = require('express-session');
            var cookieParser = require('cookie-parser');
            app.use(cookieParser());
            if (!name)
                name = "connect.sid";
            name = name + ":" + _this.port;
            ({
                postgres: function () {
                    var pgSession = require('connect-pg-simple')(session);
                    app.use(session({
                        name: name,
                        secret: secret,
                        resave: resave,
                        saveUninitialized: saveUninitialized,
                        cookie: cookie,
                        store: new pgSession(storeConfig),
                    }));
                }
            })[type]();
        };
    };
    ServerAppBuilder.prototype.offPrepareProtocol = function (protocol) {
        delete this._prepare.on[protocol];
    };
    ServerAppBuilder.prototype.offPrepareApp = function (config) {
        if (config)
            delete this._prepare.app[config];
        else
            delete this._prepare.app;
    };
    ServerAppBuilder.prototype.withoutBodyParser = function () {
        delete this._prepare.with["body-parser"];
    };
    ServerAppBuilder.prototype.onPrepareApp = function (confName, callback) {
        this._prepare.app[confName] = callback;
    };
    Object.defineProperty(ServerAppBuilder.prototype, "onPrepareHttps", {
        set: function (listener) {
            if (!Array.isArray(this._prepare.on["https"]))
                this._prepare.on["https"] = [];
            this._prepare.on["https"].push(listener);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "onPrepareHttp", {
        set: function (listener) {
            if (!Array.isArray(this._prepare.on["https"]))
                this._prepare.on["https"] = [];
            this._prepare.on["http"].push(listener);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServerAppBuilder.prototype, "onListener", {
        set: function (listener) {
            this._prepare.on["listener"] = listener;
        },
        enumerable: false,
        configurable: true
    });
    ServerAppBuilder.prototype.build = function () {
        var _this = this;
        var self = this;
        var port = self.port;
        var protocolName = this.protocolName || "http";
        var application = this.application || "app";
        var options = this.options;
        var protocol = require(this.protocolName);
        if (this.debug)
            console.log("building service " + application, protocolName + "://127.0.0.1:" + port);
        var app = express_1.default();
        this._app = app;
        this._statics.forEach(function (value) {
            app.use(express_1.default.static(value));
        });
        Object.keys(this._prepare.with).forEach(function (key) {
            if (typeof _this._prepare.with[key] === "function")
                _this._prepare.with[key]({ app: app });
        });
        Object.keys(this._prepare.app).forEach(function (key) {
            _this._prepare.app[key]({ app: app });
        });
        this._prepare.on[self.protocolName].forEach(function (listener) {
            if (typeof listener === "function")
                listener({ app: app, options: options });
        });
        var server;
        if (this.protocolName === "http") {
            server = require("http").createServer(options, app);
        }
        else {
            server = require("https").createServer(options, app);
        }
        // /**@type { Http2Server | Server}*/
        // const server:http.Server | https.Server = protocol.createServer( options, app );
        // http.createServer()
        this._server = server;
        this._server.listen(port, function () {
            var _a;
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            if (typeof _this._prepare.on["listener"] === "function")
                (_a = _this._prepare.on)["listener"].apply(_a, __spreadArray([{ app: app, server: server }], args));
            if (_this.debug)
                console.log.apply(console, __spreadArray(["running service " + application + " on", protocolName + "://127.0.0.1:" + port, "...args:"], args));
        });
        if (this.debug)
            console.log("building service " + application, protocolName + "://127.0.0.1:" + port, "ok...");
        return { app: app, server: server };
    };
    return ServerAppBuilder;
}());
exports.ServerAppBuilder = ServerAppBuilder;
//# sourceMappingURL=server-app-builder.js.map