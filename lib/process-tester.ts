export type ProcessType = void|any|boolean;
export type TestCallback = ( )=> ProcessType;
export type TestWhile = { reverse?:boolean };
export type PusherCallback = { push( callback: TestCallback ):ProcessTester }
export type Test = {
    result: boolean,
    process:TestCallback
};


export class ProcessTester{

    private _tests:Test[] = [];

    private _true = ( callback:()=>ProcessType ):ProcessTester =>{
        return this._push( true, callback );
    }

    private _false = ( callback:()=>ProcessType ):ProcessTester =>{
        return this._push( false, callback );
    }


    private _push = ( result:boolean,  process:()=>ProcessType ):ProcessTester =>{
        this._tests.push( { result, process } );
        return this;
    }

    check( condition:boolean ): PusherCallback {
        return { push: condition? this._true : this._false };
    }

    testAll(){
        const trues:TestCallback[] = this.trues;
        trues.forEach( value => {
            if( typeof value === "function" ) value()
        });
        return trues;
    }

    testFirst(){
        const trues:TestCallback[] = this.trues;
        const callback = trues[0];
        if( typeof callback === "function" ) callback();
        return callback;
    }

    testLast(){
        const trues = this.trues;
        const callback = trues[ this.trues.length -1];
        if( typeof callback === "function" ) callback();
        return callback;
    }

    testWhile( opts:TestWhile = { reverse: false }){
        const list = opts.reverse? this._tests.reverse() : this._tests;
        list.some( ( value, index) => {
            if( value.result && typeof value.process === "function" ){
                value.process();
            }
            return !value.result;
        });
    }

    testWhileFound( opts:TestWhile = { reverse: false }){
        let found;
        const list:Test[] = opts.reverse? this._tests.reverse() : this._tests;
        list.some( ( value, index) => {
            if( value.result && typeof value.process === "function" ){
                found = true;
                value.process();
            }
            return found && !value.result;
        });
    }

    get trues():TestCallback[]{
        const list:TestCallback[] = [];
        this._tests.forEach( value => {
            if( value.result ) list.push( value.process );
        });
        return list;
    }

    get falses():TestCallback[] {
        const list:TestCallback[] = [];
        this._tests.forEach( value => {
            if( !value.result ) list.push( value.process );
        });
        return list;
    }

    clean(){this._tests.length = 0;}
}