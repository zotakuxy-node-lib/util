import express, {Express} from "express";
const { ProxyUtil } = require( './proxy-util' );
import http from "http";
import https from "https";
import { Pool } from "pg";
export type Server = http.Server | https.Server;

export const acceptAny = function( res, req, next ){
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', req.method );

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true );
}

export class ServerAppBuilder {
    private _port;
    private _protocolName;
    private _application;

    // /**@type { { function}[] }*/
    private _statics: string[] = [];
    private _prepare = ProxyUtil.infinityProxyObject();
    private _options = {};

    private _app?:Express;
    private _debug = false;

    private _server?:Server;

    get app():Express|undefined { return this._app; }

    get server():Server|undefined { return this._server; }

    set port( port ) { this._port = port; }

    constructor( port, protocol = "http" ) {
        this._port = port;
        this._protocolName = protocol;
        this._prepare.on = ProxyUtil.infinityProxyArray();
    }

    get protocolName(){ return this._protocolName; }
    set protocolName( protocol ){ this._protocolName = protocol; }
    get port(){ return this._port; }
    get options(){ return this._options; }
    get application(){ return this._application; }
    set  application( application ){ this._application = application; }
    get debug() { return this._debug; }
    set debug( debug ) { this._debug = debug; }

    statics( ...statics ):ServerAppBuilder{
        this._statics.push( ...statics );
        return this;
    }

    withCors( opts ){
        this._prepare.with[ "cors" ] = ( args:{ app:Express })=>{
            const { app } = ( args|| { } );
            const cors = require( 'cors' )
            app.use( cors( opts ) );
        }
    }

    withBodyParser( opts:{ json?:{ limit? }, urlencoded?:{ extended:boolean }, raw?, text? } ){
        opts = ProxyUtil.infinityProxyObject( opts );
        this._prepare.with[ "body-parser" ] = ( { app }) =>{
            console.log( "with body-parser" );
            const bodyParser = require( 'body-parser' );

            Object.keys( opts ).forEach( key => {
                if( !ProxyUtil.isInfinity( opts[ key ] ) ) {
                    if( this.debug ) console.log( "app.use", "body-parser", key );
                    app.use( bodyParser[ key] ( opts[ key ] ) );
                }
            });

            if( !ProxyUtil.isInfinity( opts.json ) ) app.use( bodyParser.json( opts.json ) );
            if( !ProxyUtil.isInfinity( opts.urlencoded ) ) app.use( bodyParser.urlencoded( opts.json ) );
            if( !ProxyUtil.isInfinity( opts.raw ) ) app.use( bodyParser.raw( opts.raw ) );
            if( !ProxyUtil.isInfinity( opts.text ) ) app.use( bodyParser.text( opts.text ) );
        }
    }

    withSession( args: { type:"postgres", name?, secret?:string, resave?:boolean, saveUninitialized?:boolean, storeConfig?:{ pool:Pool, tableName?:string, schemaName?:string }, cookie?:{ maxAge } }){
        let { type, name, secret, resave, saveUninitialized, storeConfig, cookie } = ( args || { } );
        this._prepare.with[ "session" ] = ( { app } )=>{
            const session = require ( 'express-session' );
            const cookieParser = require( 'cookie-parser' );
            app.use( cookieParser() );
            if( !name ) name = "connect.sid";
            name = `${ name }:${ this.port }`;

            ({
                postgres(){
                    const pgSession = require( 'connect-pg-simple' )( session );
                    app.use( session({
                        name,
                        secret,
                        resave,
                        saveUninitialized,
                        cookie,
                        store: new pgSession( storeConfig ),
                    }));
                }
            })[ type ]();
        }
    }

    offPrepareProtocol( protocol ) {
        delete this._prepare.on[ protocol ];
    }

    offPrepareApp( config ){
        if( config ) delete this._prepare.app[ config ];
        else delete this._prepare.app;
    }

    withoutBodyParser( ) {
        delete this._prepare.with[ "body-parser" ];
    }

    onPrepareApp ( confName, callback:( args:({ app:Express } ) )=> void ) {
        this._prepare.app[ confName ] = callback;
    }

    set onPrepareHttps( listener:(args: { app:Express, options:any })=> void ){
        if( !Array.isArray( this._prepare.on[ "https" ] ) ) this._prepare.on[ "https" ] = [];
        this._prepare.on[ "https" ].push( listener );
    }

    set onPrepareHttp( listener:( args: { app:Express, options }) => void ){
        if( !Array.isArray( this._prepare.on[ "https" ] ) ) this._prepare.on[ "https" ] = [];
        this._prepare.on[ "http" ].push( listener );
    }

    set onListener ( listener:( args:{ app:Express, server })=>void ){
        this._prepare.on[ "listener" ] = listener;
    }

    build():{ app: Express, server: Server }{
        const self = this;
        const port = self.port;
        const protocolName = this.protocolName || "http";
        const application = this.application || "app";
        const options = this.options;
        const protocol = require( this.protocolName );

        if( this.debug ) console.log( `building service ${ application }`, `${ protocolName }://127.0.0.1:${ port }` );

        const app:Express = express();

        this._app = app;
        this._statics.forEach( value => {
            app.use( express.static( value ) );
        });

        Object.keys( this._prepare.with ).forEach( key => {
            if( typeof this._prepare.with[ key ] === "function" ) this._prepare.with[ key ]( { app } );
        });

        Object.keys( this._prepare.app ).forEach( key => {
            this._prepare.app[ key ]( { app } );
        });

        this._prepare.on[ self.protocolName ].forEach( listener =>{
            if( typeof listener === "function" ) listener( { app, options } );
        });

        let server:Server;
        if( this.protocolName === "http" ){
            server = require( "http" ).createServer( options, app );
        } else {
            server = require( "https" ).createServer( options, app );
        }

        // /**@type { Http2Server | Server}*/
        // const server:http.Server | https.Server = protocol.createServer( options, app );
        // http.createServer()

        this._server = server;

        this._server.listen( port,  ( ...args ) => {
            if( typeof this._prepare.on[ "listener" ] === "function" ) this._prepare.on[ "listener" ]( { app, server }, ...args );
            if( this.debug ) console.log(`running service ${ application } on`, `${ protocolName }://127.0.0.1:${ port }`, "...args:", ...args );
        });

        if( this.debug ) console.log( `building service ${ application }`, `${ protocolName }://127.0.0.1:${ port }`, "ok..." );

        return  { app, server };
    }
}