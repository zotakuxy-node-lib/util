"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Arguments = void 0;
var command_line_commands_1 = __importDefault(require("command-line-commands"));
var command_line_args_1 = __importDefault(require("command-line-args"));
var Arguments = /** @class */ (function () {
    function Arguments(partial) {
        if (partial === void 0) { partial = true; }
        this._optionsDefinitions = [];
        this._commands = [];
        this._partial = partial;
    }
    Object.defineProperty(Arguments.prototype, "argument", {
        set: function (_a) {
            var name = _a.name, alias = _a.alias, type = _a.type, multiple = _a.multiple, value = _a.value, required = _a.required;
            this._optionsDefinitions.push({ name: name, alias: alias, type: type, multiple: multiple, value: value, required: required });
        },
        enumerable: false,
        configurable: true
    });
    Arguments.prototype.define = function (args) {
        this.argument = { name: args.name, alias: args.alias, type: args.type, multiple: args.multiple, value: args.value, required: args.required };
    };
    Arguments.prototype.defineCommand = function (args) {
        if (typeof args === "function") {
            this._defineCommand({ callback: args, any: true, name: [null] });
        }
        else {
            if (!Array.isArray(args.name))
                args.name = [!(args === null || args === void 0 ? void 0 : args.name) ? null : args === null || args === void 0 ? void 0 : args.name];
            if (!args.callback && typeof arguments[0] === "function")
                args.callback = arguments["0"];
            this._defineCommand(Object.assign({ any: false }, args));
        }
    };
    Arguments.prototype._defineCommand = function (args) {
        var _this = this;
        if (args && args.name && Array.isArray(args.name)) {
            args.name.forEach(function (value) {
                _this._commands.push(args);
            });
        }
    };
    Object.defineProperty(Arguments.prototype, "command", {
        get: function () {
            var validCommands = [null];
            this._commands.forEach(function (value, index) {
                if (!Array.isArray(value.name))
                    validCommands.push(value.name ? value.name : null);
                else
                    value.name.forEach(function (commandName) { return validCommands.push(commandName); });
            });
            var _a = command_line_commands_1.default(validCommands), command = _a.command, argv = _a.argv;
            var options = this.values;
            var params = [];
            var optionMultipleIndex = 0;
            var currentOption;
            var optionName;
            var optionType;
            for (var i = 0; i < argv.length; i++) {
                var next = argv[i];
                var isOptions = next && next.charAt(0) === "-";
                if (isOptions) {
                    var parts = next.split("-");
                    if (parts.length > 2 && parts[0] === "" && parts[1] === "") {
                        optionType = "name";
                        optionName = next.substr(2, next.length - 2);
                        currentOption = this.optionsDefinitions.find(function (value) {
                            return value.name === optionName;
                        });
                        optionMultipleIndex = 0;
                    }
                    else {
                        optionType = "alias";
                        optionName = next.charAt(next.length - 1);
                        currentOption = this.optionsDefinitions.find(function (value) {
                            return value.alias === optionName;
                        });
                        optionMultipleIndex = 0;
                    }
                }
                if (currentOption && currentOption.type === Boolean) {
                    currentOption = undefined;
                } /*else if( currentOption && currentOption.multiple && !isOptions ){
                        if( options[currentOption.name][ optionMultipleIndex++ ] !== next ){
                            params.push( next );
                            currentOption = undefined;
                        }
                    } */
                else if (currentOption && !isOptions && !currentOption.multiple) {
                    currentOption = undefined;
                }
                else if (!currentOption && !isOptions) {
                    params.push(next);
                }
            }
            return { command: command, options: options, argv: argv, params: params };
        },
        enumerable: false,
        configurable: true
    });
    Arguments.prototype.execute = function () {
        var _a = this.command, command = _a.command, options = _a.options, argv = _a.argv, params = _a.params;
        var defs = this._commands.filter(function (value, index) {
            return (value.name && command && value.name.includes(command)
                || !command && value.name && Array.isArray(value.name) && value.name.find(function (name) { return !name; })
                || (!value.name && !command)
                || value.any) && typeof value.callback === "function";
        });
        if (defs.length > 0) {
            defs.forEach(function (value) { return value.callback ? value.callback({ command: command, options: options, argv: argv, params: params }) : null; });
        }
        else if (this._partial) {
            console.warn("No command found to execute");
        }
        else
            throw new Error("No command found to execute");
    };
    Arguments.prototype.get = function (name) {
        var _this = this;
        if (!this._options) {
            this._options = command_line_args_1.default(this._optionsDefinitions, { partial: true });
            this._optionsDefinitions.forEach(function (value) {
                if (value.required && _this._options[value.name] === undefined) {
                    throw new Error("--" + value.name + " is required argument");
                }
            });
        }
        var value = this._options[name];
        if (value === undefined) {
            // @ts-ignore
            var def = this.getDefinition(name);
            value = def.value;
        }
        return value;
    };
    Object.defineProperty(Arguments.prototype, "values", {
        get: function () {
            var _this = this;
            var values = {};
            this._optionsDefinitions.forEach(function (value, index) {
                values[value.name] = _this.get(value.name);
            });
            return values;
        },
        enumerable: false,
        configurable: true
    });
    Arguments.prototype.getDefinition = function (name) {
        if (!name)
            return this._optionsDefinitions;
        return this._optionsDefinitions.find(function (value) {
            return value.name === name;
        });
    };
    Object.defineProperty(Arguments.prototype, "optionsDefinitions", {
        get: function () {
            return this._optionsDefinitions;
        },
        enumerable: false,
        configurable: true
    });
    return Arguments;
}());
exports.Arguments = Arguments;
module.exports = { Arguments: Arguments };
//# sourceMappingURL=arguments.js.map