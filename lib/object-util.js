(()=>{

    class ObjectUtil {
        static immutable( object, ...fields ){
            if( typeof object === "object" && !Array.isArray( object ) ){
                if( !fields || fields.length === 0 ) fields = Object.keys( object );
                fields.forEach( ( field ) =>{
                    const descriptor = Object.getOwnPropertyDescriptor( object, field );
                    if( !descriptor.configurable ) return;

                    if( descriptor.value ) descriptor.writable = false;
                    descriptor.configurable = false;
                    Object.defineProperty( object, field, descriptor );
                });
            }
        }

        static immutableExcept( object, ...fields ){
            const only = [];
            Object.keys( object ).forEach( (field ) =>{
                if(fields.indexOf( field ) === -1) only.push( field );
            });
            return ObjectUtil.immutable( object, ...only );
        }

        static immutableCascade ( object ){
            ObjectUtil.immutable( object );
            if( typeof object === "object" && !Array.isArray( object ) ){
                Object.keys( object ).forEach( field => {
                    ObjectUtil.immutableCascade( object [ field ]);
                });
            }
        }

        static immutableFunctions ( object ) {
            const only = Object.keys( object ).filter( key => {
                if( typeof object[ key ] === "function" ) return true;
            });
            return ObjectUtil.immutable( object, ...only );
        }

        static set( object, name, callback ){
            if( typeof object !== "object" ) return false;
            Object.defineProperty( object, name, {
                set( v ) { callback( v )}
            })
        }

        static fields ( object, ...fields ) {
            const sub = {};
            fields.forEach( value => {
                sub[ value ]= object[ value ];
            });
            return sub;
        }
    }

    ObjectUtil.immutable( ObjectUtil );

    module.exports = { ObjectUtil };
})();
