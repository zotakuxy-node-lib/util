"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileUtil = exports.Path = exports.osSlash = void 0;
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
exports.osSlash = {
    aix: "/",
    darwin: "/",
    freebsd: "/",
    linux: "/",
    openbsd: "/",
    sunos: "/",
    win32: "\\"
};
var Path = /** @class */ (function () {
    function Path(path) {
        var _this = this;
        Object.keys(path).forEach(function (key) {
            _this[key] = path[key];
        });
    }
    Object.defineProperty(Path, "regexpDelimiters", {
        get: function () { return /[\\$]|[\/$]/; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Path, "osSep", {
        get: function () { return path_1.default.sep; },
        enumerable: false,
        configurable: true
    });
    ;
    Object.defineProperty(Path.prototype, "url", {
        get: function () { return new URL("file:" + this.path); },
        enumerable: false,
        configurable: true
    });
    return Path;
}());
exports.Path = Path;
var _loadAllFile = function (dir, filter, callback, base, args) {
    var recursive = (args || { recursive: false }).recursive;
    if (!fs_1.default.existsSync(dir)) {
        return null;
    }
    var sub;
    dir = path_1.default.resolve(dir + Path.osSep) + Path.osSep;
    if (!base)
        base = dir;
    var accepts = [];
    var files = fs_1.default.readdirSync(dir);
    var _loop_1 = function (i) {
        var file = files[i];
        var path = path_1.default.join(dir, files[i]);
        var stat = fs_1.default.lstatSync(path);
        sub = path.substr(base.length, path.length - base.length - file.length);
        var relative = "" + sub + file;
        var _path = new Path({ base: base, sub: sub, file: file, dir: dir, relative: relative, path: path });
        if (recursive && stat.isDirectory()) {
            var _paths = _loadAllFile(path, filter, callback, base, { recursive: recursive });
            if (_paths)
                accepts.push.apply(accepts, _paths);
        }
        else {
            var push = function () {
                if (callback && typeof callback === "function")
                    callback(_path);
                accepts.push(_path);
            };
            if (filter instanceof RegExp && filter.test(file))
                push();
            else if (typeof filter === "function" && filter(_path))
                push();
        }
    };
    for (var i = 0; i < files.length; i++) {
        _loop_1(i);
    }
    return accepts;
};
var FileUtil = /** @class */ (function () {
    function FileUtil() {
    }
    FileUtil.loadAllFiles = function (dir, filter, callback, args) {
        var recursive = (args || {}).recursive;
        return _loadAllFile(dir, filter, callback, undefined, { recursive: recursive });
    };
    FileUtil.scanFiles = FileUtil.loadAllFiles;
    return FileUtil;
}());
exports.FileUtil = FileUtil;
//# sourceMappingURL=file-util.js.map