"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorageManager = exports.StorageRequest = exports.ExtensionReg = exports.StorageResult = exports.StorageResolve = void 0;
var _path = require('path');
var _fs = require('fs');
var uuid = require('uuid');
var Path = require('./file-util').Path;
var ProxyUtil = require('./proxy-util').ProxyUtil;
/**@constructor*/
var StorageResolve = /** @class */ (function () {
    function StorageResolve(properties) {
        var _this = this;
        Object.keys(properties).forEach(function (property) {
            _this[property] = properties[property];
        });
    }
    Object.defineProperty(StorageResolve.prototype, "web", {
        get: function () { return (this.storage && this.access) ? "" + this.origin + this.access : undefined; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageResolve.prototype, "access", {
        get: function () { return (this.storage) ? _path.join(this.storage, this.reference) : undefined; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageResolve.prototype, "public", {
        get: function () {
            return {
                externalLink: this.web,
                internalLink: this.access,
                reference: this.reference,
                filename: this.filename,
                name: this.name,
                dir: this.dir,
                folder: this.folder,
                storage: this.storage,
                path: this.path,
                extension: this.extension,
            };
        },
        enumerable: false,
        configurable: true
    });
    return StorageResolve;
}());
exports.StorageResolve = StorageResolve;
var StorageResult = /** @class */ (function (_super) {
    __extends(StorageResult, _super);
    function StorageResult() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.success = false;
        return _this;
    }
    return StorageResult;
}(StorageResolve));
exports.StorageResult = StorageResult;
var ExtensionReg = /** @class */ (function () {
    function ExtensionReg(_a) {
        var type = _a.type, folder = _a.folder;
        this.type = type;
        this.folder = folder;
        this.path = type.split('.').reverse();
    }
    return ExtensionReg;
}());
exports.ExtensionReg = ExtensionReg;
var DEFAULT_PATH = "global";
var DEFAULT_EXTENSION = new ExtensionReg({ type: "any", folder: "others" });
var StorageRequest = /** @class */ (function () {
    function StorageRequest() {
    }
    return StorageRequest;
}());
exports.StorageRequest = StorageRequest;
var StorageManager = /** @class */ (function () {
    function StorageManager(storageDir, args) {
        var _this = this;
        if (args === void 0) { args = {}; }
        this._extensions = [];
        this._listeners = ProxyUtil.infinityProxyArray();
        this._lastIndex = 0;
        this._toFolder = function (str) {
            if (!str)
                return null;
            if (str.charAt(0) !== "/")
                str = "/" + str;
            if (str.charAt(str.length - 1) !== "/")
                str += "/";
            return str;
        };
        this.__onListen = function (listener, args) {
            args.path = _this._toFolder(args.path);
            var method = Array.isArray(args.method) ? args.method : args.method ? [args.method] : undefined;
            var index = _this._lastIndex++;
            var level = (args.path ? args.path.split(Path.regexpDelimiters).length : 0) * 3;
            level = level + (args.method ? 1 : 0);
            _this._listeners[listener].push({ method: method, path: args.path, check: args.check, success: args.success, level: level, index: index });
        };
        this._accept = function (files, args) {
            if (args === void 0) { args = {}; }
            args = args || {};
            var foldersPathsExts = ProxyUtil.infinityProxyArray();
            files.forEach(function (next) {
                var path = (next.resolve instanceof StorageResolve) ? next.resolve.path : null;
                var folder = (next.resolve instanceof StorageResolve) ? next.resolve.folder : null;
                var extension = (next.resolve instanceof StorageResolve) ? next.resolve.extension : null;
                foldersPathsExts[folder][path][extension].push(next);
            });
            var rejection = Object.keys(foldersPathsExts).find(function (folder) {
                var pathsExts = foldersPathsExts[folder];
                return Object.keys(pathsExts).find(function (path) {
                    var exts = pathsExts[path];
                    return Object.keys(exts).find(function (extension) {
                        var files = exts[folder];
                        var listeners = _this._sortListenerByPriority(_this._filterListenersFolder("accept", { folder: folder, extension: extension, path: path }, args.method));
                        var validatorListener = listeners.length > 0 ? listeners[0] : undefined;
                        var check = (validatorListener === null || validatorListener === void 0 ? void 0 : validatorListener.check) ? validatorListener.check({ files: files, method: args.method, req: args.req }) : null;
                        return !check;
                    });
                });
            });
            if (rejection) {
                return { status: false, code: 403, message: "Forbidden access to " + args.method + " file" };
            }
            else {
                Object.keys(foldersPathsExts).forEach(function (folder) {
                    var pathsExts = foldersPathsExts[folder];
                    return Object.keys(pathsExts).forEach(function (path) {
                        var exts = pathsExts[path];
                        return Object.keys(exts).forEach(function (extension) {
                            var files = exts[folder];
                            _this._sortListenerByPriority(_this._filterListenersFolder("accept", { folder: folder, extension: extension, path: path }, args.method))
                                .forEach(function (value) { return (value === null || value === void 0 ? void 0 : value.success) ? value === null || value === void 0 ? void 0 : value.success({ files: files, method: args.method, req: args.method }) : undefined; });
                        });
                    });
                });
                return { status: true, code: 200, message: "successfully" };
            }
        };
        this._sortListenerByPriority = function (listeners) {
            return listeners.sort(function (a, b) {
                var aFirst = -1, bFirst = 1, equal = 0;
                if (a.level < b.level)
                    return bFirst;
                else if (a.level > b.level)
                    return aFirst;
                else if (a.index < b.index)
                    return aFirst;
                else if (a.index > b.index)
                    return bFirst;
                else
                    return equal;
            });
        };
        this._filterListenersFolder = function (listener, args, method) {
            args = args || {};
            var dir = _this._toFolder(_path.join(args.folder, args.path));
            var folder = _this._toFolder(args.folder);
            var path = _this._toFolder(args.path);
            var extension = args.extension;
            return _this._listeners[listener].filter(function (value, index) {
                var useMethod = value.method || [method];
                var usePath = value.path || path;
                var useDir = value.dir || dir;
                var useFolder = value.folder || folder;
                var useExtension = value.extension || extension;
                var requestDir = useDir.length <= dir.length ? _this._toFolder(dir.substr(0, useDir.length)) : null;
                var requestFolder = useFolder.length <= folder.length ? _this._toFolder(folder.substr(0, useFolder.length)) : null;
                var requestPath = useDir.length <= dir.length ? _this._toFolder(dir.substr(requestFolder.length, useDir.length - requestFolder.length)) : null;
                return useMethod.includes(method)
                    && _path.normalize(useFolder) === _path.normalize(requestFolder)
                    && _path.normalize(usePath) === _path.normalize(requestPath)
                    && _path.normalize(useDir) === _path.normalize(requestDir)
                    && _path.normalize(useExtension) === _path.normalize(extension);
            });
        };
        this._uploadProcess = function (req, args) {
            var _a = (args || {}), name = _a.name, path = _a.path, useFileName = _a.useFileName, override = _a.override, storage = _a.storage, origin = _a.origin;
            /**@type {{ field, extension, reject, name, file, fileName, resolve:StorageResolve||undefined, accept }[]}*/
            var files = [];
            Object.keys(req.files).forEach(function (key) {
                var upload = req.files[key];
                if (!Array.isArray(upload))
                    upload = [upload];
                upload.forEach(function (nextFile) {
                    var _a, _b;
                    var extension = (_a = _this.extensionOf(nextFile.name)) === null || _a === void 0 ? void 0 : _a.type;
                    if (!extension)
                        extension = (_b = _this.extensionOfMimetype(nextFile.mimetype)) === null || _b === void 0 ? void 0 : _b.type;
                    var fName = !!extension ? nextFile.name.substr(0, nextFile.name.length - extension.length - 1) : null;
                    var accept = !!extension;
                    if (!accept) {
                        extension = "rejection";
                        fName = nextFile.name;
                    }
                    var file = {
                        fileName: nextFile.name,
                        field: key,
                        file: nextFile,
                        extension: extension,
                        name: fName,
                        accept: accept
                    };
                    files.push(file);
                });
            });
            files.forEach(function (next, index) {
                if (!next.accept)
                    return;
                var resolve;
                if (name)
                    _this.resolve({ path: path, name: name, extension: next.extension });
                else if (useFileName)
                    resolve = _this.resolve({ path: path, name: next.name, extension: next.extension });
                else
                    resolve = _this.createName({ extension: next.extension, path: path });
                next.accept = next.accept && (!resolve.exists || override) && !files.find(function (value, fIndex) {
                    return value && value.resolve && index !== fIndex && value.resolve.name === resolve.name;
                });
                next.resolve = resolve;
            });
            var rejected = files.find(function (value) { return !value.accept; });
            var accept = !rejected;
            return { accept: accept, files: files };
        };
        this._acceptFiles = function (files, storage, origin) { return __awaiter(_this, void 0, void 0, function () {
            var publicFiles, i, value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        publicFiles = [];
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < files.length)) return [3 /*break*/, 5];
                        value = files[i];
                        if (!(value.resolve && value.resolve instanceof StorageResolve)) return [3 /*break*/, 3];
                        return [4 /*yield*/, value.file.mv(value.resolve.resolve)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        value.file = undefined;
                        value.resolve = Object.assign(value.resolve, { storage: storage, origin: origin });
                        value.resolve = this.resolve(value.resolve).public;
                        publicFiles.push(value);
                        _a.label = 4;
                    case 4:
                        i++;
                        return [3 /*break*/, 1];
                    case 5: return [2 /*return*/, publicFiles];
                }
            });
        }); };
        this._rejectFiles = function (files) { return __awaiter(_this, void 0, void 0, function () {
            var rejectionFileName, i, value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        rejectionFileName = _path.join(this.rejectionDir, uuid.v4());
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < files.length)) return [3 /*break*/, 4];
                        value = files[i];
                        return [4 /*yield*/, value.file.mv(rejectionFileName)];
                    case 2:
                        _a.sent();
                        value.file = null;
                        value.resolve = undefined;
                        if (_fs.existsSync(rejectionFileName))
                            _fs.unlinkSync(rejectionFileName);
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        args = (args || {});
        this._storageDir = storageDir;
        this._defaultPath = args.defaultPath || DEFAULT_PATH;
        this._defaultExtension = DEFAULT_EXTENSION;
        this._defaultStorageRoute = args.defaultStorageRoute;
        _fs.mkdirSync(this.storageDir, { recursive: true });
        _fs.mkdirSync(this.dataDir, { recursive: true });
        _fs.mkdirSync(this.trashDir, { recursive: true });
        _fs.mkdirSync(this.tmpDir, { recursive: true });
        _fs.mkdirSync(this.rejectionDir, { recursive: true });
        this.define({ type: "rejection", folder: "rejected" });
    }
    Object.defineProperty(StorageManager.prototype, "defaultPath", {
        get: function () { return this._defaultPath; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "defaultStorageRoute", {
        get: function () { return this._defaultStorageRoute; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "defaultExtension", {
        get: function () { return this._defaultExtension; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "storageDir", {
        get: function () { return this._storageDir; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "dataDir", {
        get: function () { return _path.join(this.storageDir, 'data'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "tmpDir", {
        get: function () { return _path.join(this.storageDir, 'tmp'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "rejectionDir", {
        get: function () { return _path.join(this.storageDir, 'rejected'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StorageManager.prototype, "trashDir", {
        get: function () { return _path.join(this.storageDir, 'trash'); },
        enumerable: false,
        configurable: true
    });
    StorageManager.prototype.accept = function (args) {
        if (typeof args.check !== "function")
            throw new Error("Check is not function");
        this.__onListen("accept", args);
    };
    StorageManager.prototype.on = function (event) {
        this.__onListen("on", { method: event.method, path: event.path, success: event.success });
    };
    StorageManager.prototype.resolve = function (args) {
        if (args === void 0) { args = {}; }
        var _a = (args || {}), path = _a.path, name = _a.name, extension = _a.extension, stat = _a.stat, storage = _a.storage, origin = _a.origin;
        if (!path)
            path = this.defaultPath;
        extension = this._extensions.find(function (value, index) { return value.type === extension; });
        if (!extension)
            extension = this.defaultExtension;
        var proxy = new Proxy({ path: path, name: name }, {
            get: function (target, p, receiver) {
                if (target[p] === null || target[p] === undefined)
                    return "";
                return target[p];
            }
        });
        var filename = proxy.name + "." + extension.type;
        var resolve = _path.join(this.dataDir, extension.folder, proxy.path, filename);
        var dirname = _path.join(this.dataDir, extension.folder, proxy.path);
        var exists = _fs.existsSync(resolve);
        var existsDir = _fs.existsSync(dirname);
        var dir = dirname.substr(this.dataDir.length, dirname.length - (this.dataDir.length));
        var reference = resolve.substr(this.dataDir.length, resolve.length - (this.dataDir.length));
        stat = true;
        if (stat)
            stat = {};
        if (stat && exists)
            stat = _fs.statSync(resolve);
        return new StorageResolve({
            filename: filename, reference: reference, exists: exists, existsDir: existsDir, name: name, path: path, dirname: dirname, resolve: resolve, stat: stat, storage: storage, origin: origin,
            extension: extension.type,
            folder: extension.folder,
            dir: dir
        });
    };
    StorageManager.prototype.save = function (args, config) {
        if (args === void 0) { args = {}; }
        if (config === void 0) { config = {}; }
        var _a = (args || {}), path = _a.path, name = _a.name, extension = _a.extension, data = _a.data, override = _a.override, empty = _a.empty;
        var _b = (config || {}), encoding = _b.encoding, flag = _b.flag, mode = _b.mode;
        if (!name)
            args.name = this.createName({ extension: args.extension, path: args.path }).name;
        var resolve = this.resolve({ path: args.path, extension: args.extension, name: args.name });
        var success, message;
        if (!data && empty)
            data = "";
        if (!resolve.exists || override) {
            if (!resolve.existsDir)
                _fs.mkdirSync(resolve.dir, { recursive: true });
            _fs.writeFileSync(resolve.resolve, data, { encoding: encoding, flag: flag, mode: mode });
            override = resolve.exists;
            success = true;
            message = "successfully";
        }
        else {
            success = false;
            message = "file already exists";
        }
        resolve.stat = this.resolve({ stat: true, path: path, extension: extension, name: name }).stat;
        Object.assign(resolve, { success: success, message: message, override: override });
        return new StorageResult(resolve);
    };
    StorageManager.prototype.load = function (args, callback, config) {
        if (args === void 0) { args = {}; }
        if (callback === void 0) { callback = null; }
        if (config === void 0) { config = {}; }
        var _a = (args || {}), path = _a.path, name = _a.name, extension = _a.extension;
        var _b = (config || {}), flag = _b.flag, encoding = _b.encoding;
        var resolve = this.resolve({ path: path, extension: extension, name: name });
        if (resolve && resolve.exists) {
            if (callback && typeof callback === "function") {
                return _fs.readFile(resolve.resolve, function (err, buf) {
                    callback(err, buf, resolve);
                });
            }
            else
                return _fs.readFileSync(resolve.resolve, { encoding: encoding, flag: flag });
        }
        return false;
    };
    StorageManager.prototype.loadReference = function (url, callback, config) {
        if (config === void 0) { config = {}; }
        var _a = (config || {}), encoding = _a.encoding, flag = _a.flag;
        var resolve = this.exists(url);
        if (resolve && resolve instanceof StorageResolve && resolve.exists) {
            return this.load(resolve, callback, { flag: flag, encoding: encoding });
        }
        else
            return false;
    };
    StorageManager.prototype.exists = function (reference, opts) {
        if (opts === void 0) { opts = {}; }
        opts = opts || {};
        if (reference.charAt(0) === Path.osSep)
            reference = reference.substr(1, reference.length - 1);
        if (!opts)
            opts = { resolve: false };
        var parts = reference.split(Path.osSep);
        if (parts.length < 3)
            throw new Error('invalid reference url, missing parts on url');
        var folder = parts[0];
        var filename = parts[parts.length - 1];
        var fileParts = filename.split(".").reverse();
        if (fileParts.length < 2)
            throw new Error("invalid reference url, missing extension on file name");
        var fileExtension = this.extensionOf(filename);
        if (!fileExtension)
            throw new Error("invalid reference url, file extension not supported " + filename);
        var extension = fileExtension && fileExtension.folder === folder ? fileExtension.type : null;
        if (!extension)
            throw new Error("invalid reference url, this is not the extension folder");
        var name = fileParts.filter(function (value, index) { var _a; return (fileExtension === null || fileExtension === void 0 ? void 0 : fileExtension.path) && index >= ((_a = fileExtension === null || fileExtension === void 0 ? void 0 : fileExtension.path) === null || _a === void 0 ? void 0 : _a.length); }).reverse().join(".");
        var path = parts.filter(function (value, index) { return index > 0 && index + 1 < parts.length; }).join(Path.osSep);
        if (!path || path.length === 0)
            throw new Error("invalid reference url, path not present");
        var resolve = this.resolve({ path: path, extension: extension, name: name });
        if (opts.resolve)
            return resolve;
        else if (resolve.exists)
            return resolve;
        else
            return false;
    };
    StorageManager.prototype.isReference = function (url) {
        var resolve = false;
        try {
            resolve = this.exists(url, { resolve: true });
        }
        catch (e) { }
        return resolve;
    };
    StorageManager.prototype.drop = function (args, callback) {
        if (args === void 0) { args = {}; }
        if (callback === void 0) { callback = null; }
        var _a = (args || {}), name = _a.name, extension = _a.extension, path = _a.path;
        var resolve = this.resolve({ name: name, extension: extension, path: path });
        if (resolve && resolve.exists) {
            if (callback && typeof callback === "function")
                this.load(resolve, callback);
            _fs.unlinkSync(resolve.resolve);
            return resolve;
        }
        else
            return false;
    };
    /**
     * @param url
     * @param callback { function ( err: ErrnoException, data: Buffer, resolve: StorageResolve )}
     * @returns {boolean|StorageResolve}
     */
    StorageManager.prototype.dropReference = function (url, callback) {
        if (callback === void 0) { callback = null; }
        var resolve = this.exists(url);
        if (resolve && resolve instanceof StorageResolve && resolve.exists)
            return this.drop(resolve, callback);
        else
            return false;
    };
    StorageManager.prototype.createName = function (args) {
        if (args === void 0) { args = { extension: undefined }; }
        var _a = (args || {}), extension = _a.extension, path = _a.path;
        var name = uuid.v4();
        var data;
        while ((data = this.resolve({ path: path, name: name, extension: extension })).exists) {
            name = uuid.v4();
        }
        return data;
    };
    StorageManager.prototype.saveRequest = function (req, path, args) {
        var _a;
        if (args === void 0) { args = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var storageRoute, method, origin, result, name, extension, reference, pathSep, fileName, _b, files, accept;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        storageRoute = (args || {}).storageRoute;
                        method = req.method.toUpperCase();
                        origin = this.originOf(req);
                        result = { status: false };
                        storageRoute = storageRoute || this.defaultStorageRoute;
                        if (method === "PUT") {
                            reference = path;
                            pathSep = reference.split(Path.regexpDelimiters);
                            path = pathSep.length > 1 ? pathSep.filter(function (value, index) { return index > 1; }) : null;
                            fileName = pathSep.length > 2 ? pathSep[pathSep.length - 1] : null;
                            extension = fileName ? (_a = this.extensionOf(fileName)) === null || _a === void 0 ? void 0 : _a.type : undefined;
                            name = extension ? fileName.substr(0, fileName.length - extension.length - 1) : null;
                            if (!path || !extension || !name) {
                                return [2 /*return*/, Object.assign(result, { code: 400, message: "Invalid file reference" })];
                            }
                        }
                        if (!req.files || req.files.length === 0)
                            return [2 /*return*/, Object.assign(result, { code: 400, message: "No file uploaded" })];
                        if (path.charAt(0) !== "/")
                            path = "/" + path;
                        _b = this._uploadProcess(req, { name: name, path: path, origin: origin, storage: storageRoute }), files = _b.files, accept = _b.accept;
                        if (!(method === "PUT" && (files.length > 1 || files[0].extension !== extension))) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._rejectFiles(files)];
                    case 1:
                        _c.sent();
                        return [2 /*return*/, Object.assign(result, { code: 400, message: "Multiples file update not allowed or extension not compatible!", files: files })];
                    case 2:
                        Object.assign(result, { code: 403 });
                        if (!!accept) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._rejectFiles(files)];
                    case 3:
                        _c.sent();
                        return [2 /*return*/, Object.assign(result, { files: files, message: "Types of file is not supported" })];
                    case 4:
                        Object.assign(result, this._accept(files, { method: method, req: req }));
                        if (!result.status) return [3 /*break*/, 6];
                        return [4 /*yield*/, this._acceptFiles(files, storageRoute, origin)];
                    case 5:
                        _c.sent();
                        return [3 /*break*/, 8];
                    case 6: return [4 /*yield*/, this._rejectFiles(files)];
                    case 7:
                        _c.sent();
                        _c.label = 8;
                    case 8: return [2 /*return*/, Object.assign(result, { files: files })];
                }
            });
        });
    };
    StorageManager.prototype.originOf = function (req) {
        return req.protocol + "://" + req.get('host');
    };
    /**
     * @returns {((function(...[*]=))|RequestHandler[])[]}
     */
    StorageManager.prototype.listen = function (opts) {
        var _this = this;
        if (opts === void 0) { opts = {}; }
        var fileUpload = require('express-fileupload');
        if (opts.upload) {
            opts.upload.tempFileDir = this.tmpDir;
        }
        var upload = fileUpload(opts.upload || {});
        /**@type{RequestHandler[]}*/
        var handler = function (req, res, next) {
            var _a;
            var reference = req.url;
            var method = req.method.toUpperCase();
            var storage = req.baseUrl;
            var isReference = _this.isReference(reference);
            var resolve = isReference ? _this.exists(reference) : false;
            var found = isReference && reference && resolve && resolve instanceof StorageResolve && resolve.exists;
            (_a = {},
                //GET file - send a file to client
                _a["GET"] = function () {
                    if (!resolve || !(resolve instanceof StorageResolve)) {
                        res.status(isReference ? 404 : 400);
                        var message = isReference ? "File not found" : 'Invalid file reference';
                        res.json({ message: message, status: false });
                        return;
                    }
                    /**@type { {field, name, file, resolve: StorageResolve|PublicResolve, accept}[] }*/
                    var files = [{ resolve: resolve, accept: resolve.exists, name: resolve.name, fileName: resolve.filename, extension: resolve.extension }];
                    var response = _this._accept(files, { method: method, req: req });
                    if (response.status) {
                        res.sendFile(resolve.resolve);
                    }
                    else {
                        res.status(response.code);
                        res.json(response);
                    }
                },
                //DELETE file - delete a file in specific destination
                _a["DELETE"] = function () { return __awaiter(_this, void 0, void 0, function () {
                    var message, files, folder, result, resPublic_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!resolve || !(resolve instanceof StorageResolve)) {
                                    res.status(isReference ? 404 : 400);
                                    message = isReference ? "File not found" : 'Invalid file reference';
                                    res.json({ message: message, status: false });
                                    return [2 /*return*/];
                                }
                                files = [{ resolve: resolve, accept: resolve.exists, name: resolve.name }];
                                folder = resolve.folder;
                                return [4 /*yield*/, this._accept(files, { method: method, req: req })];
                            case 1:
                                result = _a.sent();
                                if (result.status) {
                                    resPublic_1 = resolve.public;
                                    this.drop(resolve, function () { return res.json(resPublic_1); });
                                }
                                return [2 /*return*/];
                        }
                    });
                }); },
                //PUT file - update or create a file to specific destination
                _a["PUT"] = function () { return __awaiter(_this, void 0, void 0, function () {
                    var response;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.saveRequest(req, reference, { storageRoute: storage })];
                            case 1:
                                response = _a.sent();
                                if (response.status)
                                    res.json(response);
                                else {
                                    res.status(response === null || response === void 0 ? void 0 : response.code);
                                    res.json(response);
                                }
                                return [2 /*return*/];
                        }
                    });
                }); },
                //POST file - create a new file in storage
                _a["POST"] = function () { return __awaiter(_this, void 0, void 0, function () {
                    var response;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.saveRequest(req, reference, { storageRoute: storage })];
                            case 1:
                                response = _a.sent();
                                if (response.status)
                                    res.json(response);
                                else {
                                    res.status(response.code);
                                    res.json(response);
                                }
                                return [2 /*return*/];
                        }
                    });
                }); },
                _a)[method.toUpperCase()]();
        };
        return [upload, handler];
    };
    StorageManager.prototype.extensionOfMimetype = function (mimetype) {
        // return { type: null };
        return undefined;
    };
    StorageManager.prototype.extensionOf = function (file) {
        var parts = file.split(Path.regexpDelimiters);
        var filename = parts[parts.length - 1].toLowerCase();
        parts = filename.split('.');
        parts.reverse();
        return this._extensions.find(function (extension) {
            if ((extension === null || extension === void 0 ? void 0 : extension.path) && parts.length <= extension.path.length)
                return false;
            var diff = extension.path.find(function (value, index) { return value !== parts[index]; });
            return !diff;
        });
    };
    StorageManager.prototype.define = function (args) {
        if (args === void 0) { args = {}; }
        var _a = (args || {}), type = _a.type, folder = _a.folder;
        type = type === null || type === void 0 ? void 0 : type.toLowerCase();
        folder = folder === null || folder === void 0 ? void 0 : folder.toLowerCase();
        this._extensions.push(new ExtensionReg({ type: type, folder: folder }));
        this._extensions.sort(function (a, b) {
            var al = a.path.length;
            var bl = b.path.length;
            return al === bl ? 0 : (al > bl) ? -1 : 1;
        });
    };
    return StorageManager;
}());
exports.StorageManager = StorageManager;
//# sourceMappingURL=storage-manager.js.map