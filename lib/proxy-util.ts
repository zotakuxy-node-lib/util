  /**@constructor*/
  export class InfinityObject { }

  /**@constructor*/
  export class InfinityArray extends Array{ }

  /**
   * @param prototype
   * @param targetCreate { function():{}|[] }
   * @returns {*}
   */
  function createInfinity( prototype, targetCreate) {
    const inf = new Proxy( targetCreate(), {
      get( target, p, receiver) {
        let value = target[ p ];
        if ( value === undefined ){
          value = createInfinity( prototype, targetCreate );
          target[ p ] = value;
        }
        return target[p];
      }
    });
    Object.setPrototypeOf( inf, prototype );
    return inf;
  }


/**@constructor*/
export class ProxyUtil {

  /**
   * @param target { *| undefined | null}
   * @returns {{}|*}
   */
  static infinityProxyObject( target? ){

    return  createInfinity( InfinityObject.prototype, () => {
      if( !target ) target = {};
      else if( typeof target !== "object" ) throw new Error( "target is not object");
      return Object.assign({}, target );
    });

  }

  /**
   * @param target
   * @returns {Proxy<InfinityArray>}
   */
  static infinityProxyArray( target? ){
    return  createInfinity( InfinityArray.prototype, () => {
      if( !target ) target = [];
      else if( !Array.isArray( target )) throw new Error( "target is not array");
      return [...target];
    });
  }

  /**
   * @param target
   * @returns {* |"object" | "array"}
   */
  static isInfinity( target ){
    return target instanceof InfinityObject ? "object"
      : target instanceof InfinityArray? "array": false;
  }

  /**
   * @param target
   * @returns {boolean}
   */
  static isInfinityObject( target ){
    return  ProxyUtil.isInfinity( target ) === "object" ;
  }

  /**
   * @param target
   * @returns {boolean}
   */
  static isInfinityArray( target ){
    return  ProxyUtil.isInfinity( target ) === "array" ;
  }
}
