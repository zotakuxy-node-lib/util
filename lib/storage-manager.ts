import ErrnoException = NodeJS.ErrnoException;
import fileUpload from "express-fileupload";
import {NextFunction, RequestHandler} from "express";
import path from "path";

export type StorageFileRequest = { field?, extension?, reject?, name?, file?, fileName?, resolve?:StorageResolve|PublicResolve , accept };
export type StorageUploadResult = { files: StorageFileRequest[], accept: boolean };
export type StorageMethod = "GET"|"POST"|"PUT"|"DELETE";

export type PublicResolve = {
    externalLink?: string
    internalLink?: string,
    reference: string,
    filename: string,
    name: string,
    folder: string,
    dir: string,
    storage: string,
    path: string,
    extension: string,
};

const _path = require( 'path' );
const _fs = require( 'fs' );
const uuid = require( 'uuid' );
const { Path } = require( './file-util' );
const { ProxyUtil } = require( './proxy-util' );


/**@constructor*/
export class StorageResolve {
    dirname;
    filename;
    reference;
    extension;
    exists; existsDir; name; path; folder; dir; resolve; stat; storage; origin;

    constructor( properties ) {
        Object.keys( properties ).forEach( (property) => {
            this[ property ] = properties[ property ];
        });
    }

    get web() { return ( this.storage && this.access )? `${ this.origin }/${ this.access.split( path.sep ).join( '/') }`: undefined; }
    get access() { return ( this.storage )? _path.join( this.storage, this.reference )  : undefined }

    get public():PublicResolve{
        return {
            externalLink: this.web,
            internalLink: this.access,
            reference: this.reference,
            filename: this.filename,
            name: this.name,
            dir: this.dir,
            folder: this.folder,
            storage: this.storage,
            path: this.path,
            extension: this.extension,
        }
    }
}

export class StorageResult extends StorageResolve{
    success = false;
    message;
}

export class ExtensionReg{
    type:string;
    folder:string;
    path:string[];
    constructor( { type, folder } ) {
        this.type = type;
        this.folder = folder;
        this.path = type.split('.').reverse();
    }
}

const DEFAULT_PATH = "global";
const DEFAULT_EXTENSION = new ExtensionReg( { type: "any", folder: "others" } );

export class StorageRequest {
    public method?:string;
    public req?:Request;
    public res?:Response;
    public next?:NextFunction;
    public files?:StorageFileRequest[];
}

export type StorageListener = {
    method?: StorageMethod|StorageMethod[],

    /** Folder + Path*/
    dir?:string,

    /**Folder of file type*/
    folder?:string,

    /**Sub-folder in folder type*/
    path?:string,
    check?:( request: StorageRequest )=>boolean,
    success?:( request: StorageRequest )=> void
};

export type AcceptorListener = StorageListener & {
    method?:StorageMethod[]
    level;
    index;
}


export class StorageManager {
    private readonly _storageDir;
    private readonly _defaultStorageRoute;
    private readonly _defaultExtension: ExtensionReg;
    private readonly _defaultPath;
    private _extensions:ExtensionReg[] = [];
    private _listeners: AcceptorListener[][] = ProxyUtil.infinityProxyArray();
    private _lastIndex = 0;

    constructor( storageDir, args: { defaultPath?, defaultStorageRoute? } = {}) {
        args = ( args || { } );
        this._storageDir = storageDir;
        this._defaultPath = args.defaultPath || DEFAULT_PATH;
        this._defaultExtension = DEFAULT_EXTENSION;
        this._defaultStorageRoute = args.defaultStorageRoute;

        _fs.mkdirSync( this.storageDir, { recursive: true } );
        _fs.mkdirSync( this.dataDir, { recursive: true } );
        _fs.mkdirSync( this.trashDir, { recursive: true } );
        _fs.mkdirSync( this.tmpDir, { recursive: true } );
        _fs.mkdirSync( this.rejectionDir, { recursive: true } );

        this.define( { type: "rejection", folder: "rejected" } );
    }

    get defaultPath(){ return this._defaultPath; }
    get defaultStorageRoute(){ return this._defaultStorageRoute; }
    get defaultExtension(){ return this._defaultExtension; }
    get storageDir(){ return this._storageDir; }
    get dataDir(){ return _path.join( this.storageDir, 'data' ); }
    get tmpDir(){ return _path.join( this.storageDir, 'tmp' ); }
    get rejectionDir(){ return _path.join( this.storageDir, 'rejected' ); }
    get trashDir(){ return _path.join( this.storageDir, 'trash' ); }


    private _toFolder = function( str ){
        if( !str ) return null;
        if( str.charAt(0) !== "/" ) str = "/"+str;
        if( str.charAt( str.length -1 ) !== "/" ) str += "/" ;
        return  str;
    }

    accept( args: StorageListener ){
        if( typeof args.check !== "function" ) throw  new Error( "Check is not function" );
        this.__onListen( "accept", args );
    }

    on( event : StorageListener ){
        this.__onListen( "on", { method: event.method, path: event.path, success: event.success } );
    }

    private __onListen = ( listener, args :StorageListener )=>{
        args.path = this._toFolder( args.path );
        const method:StorageMethod[]|undefined = Array.isArray( args.method )? args.method : args.method? [ args.method ]: undefined;
        let index = this._lastIndex++;
        let level = ( args.path? args.path.split( Path.regexpDelimiters ).length: 0 ) *3;
        level = level + ( args.method? 1 : 0 );
        this._listeners[ listener ].push( { method: method, path: args.path, check: args.check, success: args.success, level, index })
    }

    private _accept =  (files:StorageFileRequest[], args: { method?, req?:Request } = {} ) =>{
        args = args || { };
        const foldersPathsExts = ProxyUtil.infinityProxyArray();
        files.forEach( next => {
            const path = (next.resolve instanceof StorageResolve)? next.resolve.path: null;
            const folder = (next.resolve instanceof StorageResolve)? next.resolve.folder: null;
            const extension = (next.resolve instanceof StorageResolve)? next.resolve.extension: null;
            foldersPathsExts[ folder ][ path ][ extension ] .push( next );
        });


        const rejection = Object.keys( foldersPathsExts ).find( ( folder )=>{
            const pathsExts:[] = foldersPathsExts[ folder ];
            return Object.keys( pathsExts ).find( path => {
                const exts:[] = pathsExts[ path ];
                return Object.keys( exts ).find( extension => {
                    const files = exts[ folder ];

                    const listeners = this._sortListenerByPriority( this._filterListenersFolder( "accept", { folder, extension, path }, args.method ) );
                    const validatorListener:AcceptorListener|undefined = listeners.length>0? listeners[ 0 ] : undefined;
                    const check = validatorListener?.check? validatorListener.check( { files, method: args.method, req: args.req } ): null;
                    return !check;
                });
            });
        });

        if ( rejection ){
            return { status: false,  code: 403, message: `Forbidden access to ${ args.method } file` };
        } else {
            Object.keys( foldersPathsExts ).forEach( ( folder )=>{
                const pathsExts:[] = foldersPathsExts[ folder ];
                return Object.keys( pathsExts ).forEach( path => {
                    const exts:[] = pathsExts[ path ];
                    return Object.keys( exts ).forEach( extension => {
                        const files = exts[ folder ];

                        this._sortListenerByPriority( this._filterListenersFolder( "accept", { folder, extension, path }, args.method ) )
                            .forEach( value => value?.success? value?.success( { files, method:args.method, req:args.method } ): undefined );
                    });
                });
            });
            return { status: true, code: 200, message: "successfully" };
        }
    }

    private _sortListenerByPriority= ( listeners:AcceptorListener [] ):AcceptorListener []=>{
        return listeners.sort( ( a, b) => {
            const aFirst = -1, bFirst = 1, equal = 0;
            if( a.level < b.level ) return bFirst;
            else if( a.level > b.level ) return aFirst;
            else if( a.index < b.index ) return aFirst;
            else if( a.index > b.index ) return  bFirst;
            else return equal
        });
    }

    private _filterListenersFolder = ( listener:"on"|"accept", args:{ folder?, extension?, path? }, method:StorageMethod ):AcceptorListener[]=>{
        args = args || { };
        let dir = this._toFolder( _path.join( args.folder, args.path ) );
        let folder = this._toFolder( args.folder );
        let path = this._toFolder( args.path );
        let extension = args.extension;

        return this._listeners[ listener ].filter(( value, index) => {
            const useMethod = value.method || [ method ];
            const usePath = value.path || path;
            const useDir = value.dir || dir;
            const useFolder = value.folder || folder;
            const useExtension = value.extension || extension;

            const requestDir = useDir.length <= dir.length ? this._toFolder( dir.substr(0, useDir.length)) : null;
            const requestFolder = useFolder.length <= folder.length ? this._toFolder( folder.substr(0, useFolder.length) ) : null;
            const requestPath = useDir.length <= dir.length ? this._toFolder( dir.substr( requestFolder.length, useDir.length - requestFolder.length ) ) : null;


            return useMethod.includes( method )
                && _path.normalize( useFolder ) === _path.normalize( requestFolder )
                && _path.normalize( usePath ) === _path.normalize( requestPath )
                && _path.normalize( useDir ) === _path.normalize( requestDir )
                && _path.normalize( useExtension ) === _path.normalize( extension );
        });
    }

    resolve( args:{ path?, name?, extension?, stat?, storage?, origin? } = {} ){
        let { path, name, extension, stat, storage, origin } = ( args || { } );
        if( !path ) path = this.defaultPath;
        extension = this._extensions.find( (value, index) => value.type === extension );
        if( !extension ) extension = this.defaultExtension;

        const proxy = new Proxy({path, name }, {
            get(target, p, receiver) {
                if (target[p] === null || target[p] === undefined) return "";
                return target[ p ];
            }
        });

        const filename = `${ proxy.name }.${ extension.type }`;
        const resolve = _path.join( this.dataDir, extension.folder, proxy.path, filename );
        const dirname = _path.join( this.dataDir, extension.folder, proxy.path );

        const exists = _fs.existsSync( resolve );
        const existsDir = _fs.existsSync( dirname );
        const dir = dirname.substr( this.dataDir.length, dirname.length - ( this.dataDir.length ) );
        const reference = resolve.substr( this.dataDir.length, resolve.length - ( this.dataDir.length ) );
        stat = true;
        if( stat ) stat = {};
        if( stat && exists ) stat = _fs.statSync( resolve );

        return new StorageResolve({
            filename, reference, exists, existsDir, name, path, dirname, resolve, stat, storage, origin,
            extension: extension.type,
            folder: extension.folder,
            dir: dir
        });
    }

    save( args:{ path?, name?, extension?, data?, override?, empty? } = {}, config:{ encoding?, flag?, mode? } = {} ):StorageResult{
        let { path, name, extension, data, override, empty } = ( args || { } );
        let { encoding, flag, mode } = ( config || { } );

        if( !name ) args.name = this.createName( { extension:args.extension, path:args.path } ).name;

        let resolve = this.resolve( { path:args.path, extension:args.extension, name:args.name });
        let success, message;
        if( !data && empty) data = "";
        if( !resolve.exists || override ){
            if( !resolve.existsDir ) _fs.mkdirSync( resolve.dir, { recursive: true } );
            _fs.writeFileSync( resolve.resolve, data, { encoding, flag, mode });
            override = resolve.exists;
            success = true;
            message = "successfully";
        } else {
            success = false;
            message = "file already exists";
        }

        resolve.stat = this.resolve( {stat:true, path, extension, name } ).stat;
        Object.assign( resolve, { success, message, override } );
        return new StorageResult( resolve );
    }


    load( args:{ path?, name?, extension? } = {}, callback:((err: ErrnoException, buf: Buffer, resolve: StorageResolve)=>void )|null = null, config: { flag?, encoding? } = {} ):boolean|void|Buffer{
        let { path, name, extension } = ( args || { } );
        let { flag, encoding } = ( config || { } );

        let resolve = this.resolve( { path, extension, name });
        if( resolve && resolve.exists ){
            if( callback && typeof callback === "function" ){
                return _fs.readFile( resolve.resolve, (err, buf)=>{
                    callback ( err, buf, resolve );
                });
            }
            else return  _fs.readFileSync( resolve.resolve, { encoding, flag })
        }
        return false;
    }

    loadReference(url, callback:(( err: ErrnoException, data: Buffer, resolve: StorageResolve )=>void), config: { encoding?, flag? } = {} ){
        let { encoding, flag } = ( config || {} );
        const resolve = this.exists( url );
        if( resolve && resolve instanceof StorageResolve && resolve.exists ){
            return  this.load( resolve, callback, { flag, encoding } );

        } else return false;
    }

    exists( reference, opts:{ resolve?: boolean} = { } ):boolean|StorageResolve {
        opts = opts || { };

        if( reference.charAt(0) === Path.osSep ) reference = reference.substr( 1, reference.length-1 );
        if( !opts ) opts = { resolve: false };
        const parts = reference.split( Path.osSep );
        if( parts.length < 3 ) throw new Error( 'invalid reference url, missing parts on url' );
        const folder = parts [ 0 ];

        const filename = parts[ parts.length-1 ];
        const fileParts = filename.split( "." ).reverse();

        if( fileParts.length < 2 ) throw  new Error( "invalid reference url, missing extension on file name" );
        let fileExtension = this.extensionOf( filename );

        if(!fileExtension ) throw new Error( "invalid reference url, file extension not supported "+ filename );

        let extension = fileExtension && fileExtension.folder === folder? fileExtension.type: null;
        if( !extension ) throw new Error( "invalid reference url, this is not the extension folder" );

        const name = fileParts.filter( (value, index) => fileExtension?.path && index >= fileExtension?.path?.length  ).reverse().join( "." );
        const path = parts.filter( ( value, index) => index > 0 && index+1 < parts.length ).join( Path.osSep );
        if( !path || path.length === 0 ) throw new Error( "invalid reference url, path not present" );
        const resolve = this.resolve( { path, extension, name } );

        if ( opts.resolve ) return resolve;
        else if( resolve.exists ) return resolve;
        else return false;
    }

    isReference( url ):boolean|StorageResolve {
        let resolve:boolean|StorageResolve = false;
        try {
            resolve = this.exists( url, { resolve: true } );
        } catch (e) { }
        return resolve;
    }

    drop( args:{ name?, extension?, path? }={}, callback:(( err: ErrnoException, data: Buffer, resolve: StorageResolve )=>void)|null = null ):boolean|StorageResolve {
        const { name, extension, path } = ( args || { } );
        const  resolve = this.resolve({ name, extension, path } );
        if( resolve && resolve.exists ){
            if( callback && typeof callback === "function" ) this.load( resolve, callback );
            _fs.unlinkSync( resolve.resolve );
            return resolve;
        } else return  false;
    }

    /**
     * @param url
     * @param callback { function ( err: ErrnoException, data: Buffer, resolve: StorageResolve )}
     * @returns {boolean|StorageResolve}
     */
    dropReference( url, callback = null ){
        const resolve:StorageResolve|boolean = this.exists( url );
        if( resolve && resolve instanceof StorageResolve && resolve.exists ) return this.drop( resolve, callback );
        else return false;

    }

    createName( args :{ extension, path? } = { extension: undefined } ):StorageResolve{
        let { extension, path } = (args || {});
        let name = uuid.v4();
        let data;
        while( ( data = this.resolve( { path, name, extension } ) ).exists ){ name = uuid.v4(); }
        return data;
    }

    private _uploadProcess = ( req, args:{ name?:string, path?:string, useFileName?:boolean, override?:boolean, storage:string, origin:string } ):StorageUploadResult=>{
        let { name, path, useFileName, override, storage, origin } = ( args || { } );
        /**@type {{ field, extension, reject, name, file, fileName, resolve:StorageResolve||undefined, accept }[]}*/
        const files:StorageFileRequest[] = [];
        Object.keys( req.files ).forEach( key => {
            let upload = req.files[ key ];
            if( !Array.isArray( upload ) ) upload = [ upload ];
            upload.forEach( ( nextFile )=>{
                let extension = this.extensionOf( nextFile.name )?.type;
                if( !extension ) extension = this.extensionOfMimetype( nextFile.mimetype )?.type;
                let fName = !!extension? nextFile.name.substr( 0, nextFile.name.length - extension.length -1 ): null;
                const accept = !!extension;
                if( !accept ) {
                    extension = "rejection";
                    fName = nextFile.name;
                }

                const file:StorageFileRequest = {
                    fileName: nextFile.name,
                    field: key,
                    file: nextFile,
                    extension,
                    name: fName,
                    accept
                };
                files.push( file );
            })
        });

        files.forEach( ( next, index) => {
            if( !next.accept ) return;
            let resolve;
            if( name ) this.resolve( { path, name, extension: next.extension } );
            else if( useFileName ) resolve = this.resolve( { path, name: next.name, extension: next.extension } );
            else resolve = this.createName( { extension: next.extension, path } );
            next.accept =  next.accept && ( !resolve.exists || override ) && !files.find( (value, fIndex) => {
                return value && value.resolve && index !== fIndex && value.resolve.name === resolve.name;
            });
            next.resolve = resolve;
        });

        const rejected = files.find( value => !value.accept );
        const accept = !rejected;
        return { accept, files };
    }

    private _acceptFiles = async (files:StorageFileRequest[], storage, origin ) =>{
        const  publicFiles:StorageFileRequest[] = [];
        for ( let i = 0; i< files.length; i++  ){
            const value = files[ i ];
            if( value.resolve && value.resolve instanceof StorageResolve ) await value.file.mv( value.resolve.resolve );
            value.file = undefined;
            value.resolve = Object.assign( value.resolve, { storage, origin } );
            value.resolve = this.resolve( value.resolve ).public;
            publicFiles.push( value );
        }
        return publicFiles;
    }

    private _rejectFiles = async ( files ) =>{
        const rejectionFileName = _path.join( this.rejectionDir, uuid.v4() );
        for ( let i = 0; i< files.length; i++  ){
            const value = files[ i ];
            await value.file.mv( rejectionFileName );
            value.file = null;
            value.resolve = undefined;
            if( _fs.existsSync( rejectionFileName ) ) _fs.unlinkSync( rejectionFileName );
        }
    }

    async saveRequest( req, path, args:{ storageRoute? } = {} ):Promise<{ status: boolean, code?: number, message?: string, files?:StorageFileRequest[] }>{
        let { storageRoute } = ( args || { } );
        const method = req.method.toUpperCase();
        const origin = this.originOf( req );
        const result:{ status: boolean, message?:string, files?: StorageFileRequest[], code?:number } = { status: false };
        let name:string|undefined,
            extension:string|undefined;
        storageRoute = storageRoute || this.defaultStorageRoute;

        if( method === "PUT" ){
            const reference = path;
            const pathSep = reference.split( Path.regexpDelimiters );
            path = pathSep.length> 1 ? pathSep.filter( (value, index) => index > 1 ): null;
            const fileName = pathSep.length > 2? pathSep[ pathSep.length -1 ]: null;
            extension = fileName? this.extensionOf( fileName )?.type : undefined;
            name = extension? fileName.substr( 0, fileName.length - extension.length -1): null;
            if( !path || !extension || !name ){
                return Object.assign( result,  { code: 400, message: "Invalid file reference"  } );
            }
        }

        if( !req.files || req.files.length === 0  )
            return Object.assign(result,  { code: 400, message: "No file uploaded" } );

        if( path.charAt(0 ) !== "/" ) path = "/"+path;
        let { files, accept } = this._uploadProcess( req, { name, path, origin, storage: storageRoute } );
        if( method === "PUT" && ( files.length > 1 || files[0].extension !== extension ) ){
            await this._rejectFiles( files );
            return Object.assign( result, { code: 400, message: "Multiples file update not allowed or extension not compatible!", files });
        }

        Object.assign( result, { code: 403 } );
        if( !accept ) {
            await this._rejectFiles( files );
            return Object.assign( result, { files, message: "Types of file is not supported" } );
        }

        Object.assign( result, this._accept( files,{ method, req } ) );
        if( result.status ) await this._acceptFiles( files, storageRoute, origin );
        else await this._rejectFiles( files );
        return Object.assign( result, { files } );
    }

    originOf( req ){
        return `${req.protocol}://${req.get('host')}`;
    }
    /**
     * @returns {((function(...[*]=))|RequestHandler[])[]}
     */
    listen( opts:{ upload?: fileUpload.Options } = { } ):RequestHandler[]{
        const fileUpload = require( 'express-fileupload' );
        if( opts.upload ){
            opts.upload.tempFileDir = this.tmpDir;
        }
        const upload =  fileUpload( opts.upload || {} );

        /**@type{RequestHandler[]}*/
        let handler = ( req, res, next ) => {
            const reference = req.url.split( '/' ).join( path.sep );
            const method = req.method.toUpperCase();
            const storage = req.baseUrl;
            const isReference = this.isReference( reference );
            let resolve:StorageResolve|boolean = isReference? this.exists( reference ): false;
            const found = isReference && reference && resolve && resolve instanceof StorageResolve && resolve.exists;

            ({
                //GET file - send a file to client
                [ "GET" ]: () => {
                    if( !resolve || ! ( resolve instanceof StorageResolve )){
                        res.status( isReference? 404: 400 );
                        const message = isReference? "File not found": 'Invalid file reference';
                        res.json({ message,  status: false });
                        return;
                    }

                    /**@type { {field, name, file, resolve: StorageResolve|PublicResolve, accept}[] }*/
                    const files:StorageFileRequest[] = [ { resolve, accept: resolve.exists, name: resolve.name, fileName: resolve.filename, extension: resolve.extension } ];

                    const response = this._accept( files,{ method, req } );

                    if( response.status ){
                        res.sendFile( resolve.resolve )
                    } else{
                        res.status( response.code );
                        res.json( response );
                    }

                },

                //DELETE file - delete a file in specific destination
                [ "DELETE" ]: async ()=> {
                    if( !resolve || !( resolve instanceof StorageResolve )){
                        res.status( isReference? 404: 400 );
                        const message = isReference? "File not found": 'Invalid file reference';
                        res.json({ message,  status: false });
                        return;
                    }

                    const files:StorageFileRequest[] = [ { resolve, accept: resolve.exists, name: resolve.name } ];
                    const folder = resolve.folder;

                    const result = await this._accept( files,{ method, req } );
                    if( result.status ){
                        const resPublic = resolve.public;
                        this.drop( resolve, ()=> res.json( resPublic ) );
                    }
                },

                //PUT file - update or create a file to specific destination
                [ "PUT" ]: async ()=> {
                    const response = await this.saveRequest( req, reference, { storageRoute: storage } );
                    if( response.status ) res.json( response );
                    else{
                        res.status( response?.code );
                        res.json( response );
                    }
                },

                //POST file - create a new file in storage
                [ "POST" ]: async ()=> {
                    const response = await this.saveRequest( req, reference, { storageRoute: storage } );
                    if( response.status ) res.json( response );
                    else{
                        res.status( response.code );
                        res.json( response );
                    }
                },

            })[ method.toUpperCase() ]();
        }
        return  [upload, handler];
    }

    extensionOfMimetype( mimetype  ):ExtensionReg | undefined{
        // return { type: null };
        return undefined;

    } extensionOf(file ){
        let parts = file.split( Path.regexpDelimiters );
        const filename = parts[ parts.length-1 ].toLowerCase();
        parts = filename.split( '.' );
        parts.reverse();

        return this._extensions.find(( extension ) => {
            if ( extension?.path && parts.length <= extension.path.length ) return false;
            const diff = extension.path.find( ( value, index ) => value !== parts[ index ] )
            return !diff;
        });

    } define( args:{ type?:string, folder?:string } = { } ){
        let { type, folder } = ( args || { } );
        type = type?.toLowerCase();
        folder = folder?.toLowerCase();

        this._extensions.push( new ExtensionReg({ type, folder }) );
        this._extensions.sort( (a, b) => {
            const  al:number = a.path.length;
            const  bl:number = b.path.length;
            return al === bl ? 0 : ( al > bl ) ? -1 : 1
        });
    }
}
