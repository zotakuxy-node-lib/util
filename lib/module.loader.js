(()=>{
    const { FileUtil, Path }= require( './file-util' );
    const path = require( 'path' );

    const ModuleType = {
        module: "module",
        listener: "listener",
        init: "init",
        main: "main"
    };

    /**@constructor*/
    class ModuleFilter {
        regExp;
        name;
        level;
        dir
        constructor( props ) {
            Object.assign( this, props );
        }
    }

    const RegExpFilters = {
        module( { level, dir }={ } ){ return new ModuleFilter({
            regExp : /.*.module.js$/,
            name: "module",
            level,
            dir

        })},

        listener( { level, dir }={ } ) { return new ModuleFilter({
            regExp: /.*.listener.js$/,
            name: "listener",
            level,
            dir

        })},

        route( { level, dir }={ } ) { return new ModuleFilter({
            regExp: /.*.route.js$/,
            name: "route",
            level,
            dir

        })},

        event( { level, dir }={ } ) { return new ModuleFilter({
            regExp: /.*.event.js$/,
            name: "event",
            level,
            dir

        })},

        init( { level, dir }={ } ) { return new ModuleFilter({
            regExp: /.*.init.js$/,
            name: "init",
            level

        })},

        database( { level, dir }={ } ) { return new ModuleFilter({
            regExp: /.*.db.js$/,
            name: "database",
            level,
            dir
        })}
    }

    /**@type {RegExp[]}*/
    function skipFiles( files, dirname ) {
        const skips = [];
        files.forEach( (fileName, index) => {
            console.log( "fileName", fileName, path.resolve( dirname, fileName )  );
            skips.push( new RegExp( path.resolve( dirname, fileName ) ) );
        });
        return  skips;
    }

    /**
     * @constructor
     * @extends Path
     */
    class LoaderRef extends Path {
        name;
        loader = false;
        skip = false;
        status = false;
        value;

        /**@type LoaderRef*/
        #_parent;

        /**
         * @param path { Path }
         * @param parent { LoaderRef || * }
         */
        constructor( path , parent ) {
            super( path );
            this.name = this.file;
            this.#_parent = parent;
        }

        get route(){
            if( this.#_parent ) return `${ this.parent().route }${ Path.osSep }${this.name }`;
            else return  this.name;
        }
    }

    const Scope = {
        PUBLIC: "public",
        PRIVATE: "private",
        PROTECT: "protect"
    }


    /** @constructor*/
    class Module {

        /**
         * @param dir
         * @param filter { ModuleFilter | ModuleFilter[] }
         * @param ops { {dir, name, scope, ref, filter, start, selfSkip, type, level} }
         * @returns {Module}
         */
        static withModule(dir, filter, ops ){
            const  path = dir.split( Path.regexpDelimiters );
            ops = ops || {};
            return new Module(Object.assign({
                dir,
                name: path[path.length - 1],
                filter,
                type: ModuleType.module
            }, ops ));
        }

        #_dir;
        #_start;
        #_name;
        #_scope;
        #_type;

        /**@type { string | Number[] }*/
        #_level;

        /**@type LoaderRef*/
        #_ref;

        #_selfSkip;

        /**@type {ModuleFilter|ModuleFilter[]}*/
        #_filter;

        /** @type { RegExp[] }*/
        #_skips = [];

        /** @type {LoaderRef[]}*/
        #_children = [];

        /**@type Module*/
        #_anchor;
        #_loaded = false;

        constructor( { dir, name, scope, ref, filter, start, selfSkip, type, level } = {} ) {
            if( typeof filter === "function" ) filter = filter();
            this.#_start = start || dir;
            this.#_dir = dir || start;
            this.#_name = name;
            this.#_scope = scope;
            this.#_ref = ref;
            this.#_filter = filter;
            this.#_selfSkip = selfSkip;
            this.#_type = type;
            this.#_level = level || [ 1 ];
        }

        /**
         * @param skips
         * @returns {RegExp[]}
         */
        skips( ...skips ){
            if( skips && skips.length > 0 )
                this.#_skips.push(... skipFiles( skips, this.start ) );
            return this.#_skips;
        }

        get status(){
            if( this.runningListeners === this.countListeners ) return "ok"
            if( this.runningListeners === 0 && this.countListeners > 0 ) return "failed"
            if( this.runningListeners > 0 && this.runningListeners < this.countListeners ) return "partial";
        }

        get countListeners(){ return this.#_children.length; }

        get skipListeners(){
            return this.#_children.filter(value => {
                return value.skip;
            });
        }

        get runningListeners(){
            return this.#_children.filter(value => {
                return value.status;
            }).length;
        };

        get failedListeners(){
            return this.#_children.filter(value => {
                return value.status;
            }).length;
        };

        get dir () { return this.#_dir; }
        get start () { return this.#_start; }
        get name () { return this.#_name; }
        get children () { return this.#_children }
        get ref() { return this.#_ref; }
        get selfSkip() { return this.#_selfSkip; };
        get filter() { return this.#_filter; }
        get anchor() { return this.#_anchor; }
        get type(){ return this.#_type; }
        get level() { return this.#_level; }

        get isLoaded(){ return this.#_loaded; }

        /**@param anchor*/

        set anchor( anchor ) { this.#_anchor = anchor; }
        get route(){
            return this.#_anchor ? `${this.anchor.route}/${this.name}`
                : this.name;
        }

        toString(){return JSON.stringify({
            name: this.name,
            route: this.route,
        })}

        load(){
            const  self = this;
            if( this.isLoaded ) return;
            console.log( `${ self.type }:://${ self.route }...` );
            /**@type { ModuleFilter[]}*/
            let filters;

            if( Array.isArray( this.filter ) ) filters = this.filter;
            else filters = [ this.filter ];
            filters.forEach( value =>{

                if( value.level === null || value.level === undefined ) value.level = self.level;
                if( value.dir === null || value.dir === undefined ) value.dir = self.dir;
                this.#_loadFilter( value );
            } );
            this.#_loaded = true;
            console.log( `${ self.type }:://${ self.route }... ok` );
        }

        /** @param filter { ModuleFilter } */
        #_loadFilter = ( filter ) =>{
            const self = this;
            //language=file-reference
            FileUtil.loadAllFiles( filter.dir, filter.regExp, p => {
                const childRef = new LoaderRef( p, this.ref );
                try {
                    const parts = p.relative.split( Path.regexpDelimiters );
                    let levelNumber = parts.length -1;
                    if( !( filter.level === "*" || self.level.includes( levelNumber )) ){
                        return;
                    }

                    const find = self.skips().find(value => value.test( p.path ) );

                    if( find ){
                        console.log( `${ filter.name }:://${ self.route }`, p.url.href , "skipped..."  );
                        childRef.skip = true;

                    } else {
                        childRef.loader = true;
                        childRef.value = require( p.path );

                        if( childRef.value instanceof Module && childRef.value.selfSkip ){
                            childRef.value.anchor = self;
                            console.log( `${ filter.name }:://${ self.route }`, p.url.href , "self skipped..."  );
                            return;
                        } else if ( childRef.value instanceof Module ){
                            childRef.value.anchor = self;
                            childRef.value.load();
                        }
                        if(!( childRef.value instanceof  Module )){
                            console.log(`${ filter.name }:://${ self.route }`, p.url.href , "ok..." );
                        }
                        childRef.status = true;
                    }

                } catch (e) {
                    console.error( `${ filter.name }:://${ self.route }`, p.url.href , "failed skipped!"  );
                    console.log( e );
                }
                self.children.push( childRef );

            }, { recursive: true });
        }
    }

    const ModuleLoader = { skipFiles, Module, LoaderRef, Scope, RegExpFilters, ModuleType };
    Object.assign( ModuleLoader, { ModuleLoader } );
    ModuleLoader.withModule = Module.withModule;
    module.exports = ModuleLoader;

})();