import commandLineCommands  from "command-line-commands";
import commandLineArgs  from "command-line-args";

export type Definition = { name:string, alias?:string, type?, multiple?:boolean, value?:any, required?:boolean };

export type CommandCallback<T> = ( receiver: { command:string|null, options:T, argv:any, params:string[] } )=>any;
export type CommandDefinition<T> = { name?:string|null|(string|null)[], callback?:CommandCallback<T> };

export class Arguments <ArgType > {

    private _optionsDefinitions: Definition [] = [];
    private readonly _partial;
    private _options;
    private _commands: (CommandDefinition<ArgType>&{ any: boolean })[] = [];

    constructor( partial = true ) {
        this._partial = partial;
    }

    set argument( { name, alias, type, multiple, value, required } ){
        this._optionsDefinitions.push( { name, alias, type, multiple, value, required } );
    }

    define( args: Definition ){
        this.argument = { name:args.name, alias:args.alias, type:args.type, multiple:args.multiple, value:args.value, required:args.required  }
    }


    defineCommand ( args: CommandDefinition<ArgType>|CommandCallback<ArgType> ){
        if( typeof args === "function" ) {
            this._defineCommand( { callback: args, any: true, name: [ null ] } );
        } else {
            if( !Array.isArray( args.name ) ) args.name = [ !args?.name? null: args?.name  ];
            if( !args.callback && typeof arguments[0] === "function") args.callback = arguments[ "0" ];
            this._defineCommand(Object.assign({ any: false}, args ) );
        }
    }


    private _defineCommand( args: CommandDefinition<ArgType>&{ any: boolean }){
        if( args && args.name && Array.isArray( args.name ) ){
            args.name.forEach( (value)=>{
                this._commands.push( args );
            })
        }
    }


    get command():{ command: string|null, options: any, argv:string[], params:string[] }{
        const validCommands:(string|null)[] = [ null ];
        this._commands.forEach( (value, index) => {
            if( !Array.isArray( value.name ) ) validCommands.push( value.name? value.name: null );
            else value.name.forEach( commandName => validCommands.push( commandName ) );
        });

        const { command, argv } = commandLineCommands( validCommands );
        const options = this.values;

        const params:string[] = [];

        let optionMultipleIndex:number = 0;
        let currentOption:Definition|undefined;
        let optionName:string|undefined;
        let optionType:"name"|"alias"|undefined;
        for ( let  i =0; i< argv.length; i++ ){
            const next:string = argv[ i ];
            const isOptions = next && next.charAt(0 ) === "-";
            if( isOptions ){
                const parts = next.split( "-" );
                if( parts.length > 2 && parts[0] === "" && parts[1] === "" ){
                    optionType = "name"
                    optionName = next.substr( 2, next.length-2 );
                    currentOption = this.optionsDefinitions.find( value => {
                        return value.name === optionName;
                    });
                    optionMultipleIndex = 0;
                } else {
                    optionType = "alias";
                    optionName = next.charAt( next.length-1 );
                    currentOption = this.optionsDefinitions.find( value => {
                        return value.alias === optionName;
                    })
                    optionMultipleIndex = 0;
                }
            }

            if( currentOption && currentOption.type === Boolean ){
                currentOption = undefined;
            } /*else if( currentOption && currentOption.multiple && !isOptions ){
                    if( options[currentOption.name][ optionMultipleIndex++ ] !== next ){
                        params.push( next );
                        currentOption = undefined;
                    }
                } */else if( currentOption && !isOptions && !currentOption.multiple ){
                currentOption = undefined;
            } else if( !currentOption && !isOptions ){
                params.push( next );
            }
        }

        return { command, options, argv, params };
    }

    execute(){
        const { command, options, argv, params } = this.command;
        const defs = this._commands.filter( ( value, index) => {
            return ( value.name && command && value.name.includes( command )
                || !command && value.name && Array.isArray( value.name ) && value.name.find( (name)=> !name )
                || (!value.name && !command)
                || value.any ) && typeof value.callback === "function"
        });

        if( defs.length > 0 ){
            defs.forEach( value => value.callback? value.callback( { command, options, argv, params }  ): null );
        } else if( this._partial ) {
            console.warn( "No command found to execute" );
        } else throw new Error( "No command found to execute" )
    }

    get( name ){
        if( !this._options ){
            this._options = commandLineArgs( this._optionsDefinitions, { partial: true } );
            this._optionsDefinitions.forEach( value => {
                if( value.required && this._options[ value.name ] === undefined ){
                    throw new Error( `--${ value.name } is required argument` );
                }
            });
        }

        let value = this._options[ name ];
        if( value === undefined ){
            // @ts-ignore
            const def:Definition = this.getDefinition( name );
            value = def.value
        }
        return value;
    }

    get values( ):ArgType {
        let values:ArgType|any= {};
        this._optionsDefinitions.forEach( ( value, index) => {
            values[ value.name ] = this.get( value.name );
        });
        return values;
    }

    getDefinition( name ):Definition|Definition[]|undefined{
        if( !name ) return this._optionsDefinitions;
        return this._optionsDefinitions.find(value => {
            return value.name === name;
        })
    }

    get optionsDefinitions() {
        return this._optionsDefinitions;
    }
}

module.exports = { Arguments };
