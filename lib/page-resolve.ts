import path from 'path';
import fs from 'fs';
import { NextFunction, Request, Response, default as express } from "express";
import { default as parseurl } from 'parseurl';
import resolvePath from "resolve-path";

export interface ContentView {
    load( req: Request, res: Response, next: NextFunction ): Promise< any >
}

export function isContentView( object ): object is ContentView {
    const _interface = object as ContentView;
    return _interface?.load !== undefined && typeof _interface?.load === "function";
}

enum PageType { "dynamicPage" = ".ejs", "staticPage" = ".html" }

interface PathDescription {
    fullPath?: string,
    fullDir?: string,
    resolve?: string,
    folder?: string,
    url: string,
    withRelative: string
    relative: string,
    relativeDir: string,
    filename: string,
    name: string,
    extension: string,
    needSlash?: boolean
}

type Folders = string[] | string | null;

type ResolveMap = { folder: string, resolver: "dynamic" | "static", type: ".html" | ".ejs"| "", with: "" | "index" };
type AcceptorArgs = { pageType: PageType, description: PathDescription, data:any };
type ResolveResult = {
    code: number,
    message: string,
    status: boolean,
    canNext: boolean,
    resolved: string|null|undefined|any,
    mapper: ResolveMap|null|undefined,
    description: PathDescription|null|undefined,
    parsed: any
};



type PageResolveOptions = {
    version?: string|null|undefined,
    folders?:{
        views?: Folders,
        public?: Folders,
        contents?: string|undefined|null
    },
    dirSlash?: boolean,
    hiddenIndex?: boolean
};


export function PageResolve ( opts: PageResolveOptions  ){
    const resolvers :( ( args: PathDescription|null|undefined, req: Request, res: Response )=>boolean)[] = [];
    const acceptors :( ( args: AcceptorArgs, req: Request, res: Response )=>boolean)[] = [];

    const folders: { public, views, contents } = { public: opts?.folders?.public, views: opts?.folders?.views, contents: opts?.folders?.contents };
    const version = opts?.version;
    const dirSlash:boolean|undefined = opts.dirSlash;
    const hiddenIndex = opts.hiddenIndex;

    if( !Array.isArray( folders.public ) ) folders.public = [ folders.public ];
    if( !Array.isArray( folders.views ) ) folders.views = [ folders.views ];

    const foldersScan:ResolveMap [] = [];

    type FolderCheck = { folders:string[], callback:( folder )=> any };
    ([
        //Find html in public dirs
        { folders: folders.public, callback:( folder )=> foldersScan.push( { folder, type: ".html", resolver: "static", with: "" } ) },

        //Find views in dynamics dirs
        { folders: folders.views, callback:( folder )=> foldersScan.push( { folder, type: ".ejs", resolver: "dynamic", with: "" } ) },

        //Find other file in public dirs
        { folders: folders.public, callback:( folder )=> foldersScan.push( { folder, type: "", resolver: "static", with: "" } ) },

        //Find index/html in public dirs
        { folders: folders.public, callback:( folder )=> foldersScan.push( { folder, type: ".html", resolver: "static", with: "index" } ) },

        //Find index/ejs in views dirs
        { folders: folders.views, callback:( folder )=> foldersScan.push( { folder, type: ".ejs", resolver: "dynamic", with: "index" } ) },
    ]).forEach( ( check: FolderCheck ) => {
        if( check.folders ) check.folders.forEach( folder => check.callback( folder )) ;
    });

    const resolveRequest = ( req: express.Request ): ResolveResult =>{
        // get the pathname from the URL (decoded )
        const parsed = parseurl( req );

        const pathName = !!( parsed?.pathname )? decodeURIComponent( parsed?.pathname ): null;
        if ( !pathName ) return {
            code: 400,
            message: "path required",
            status: false,
            canNext: false,
            mapper: null,
            resolved: null,
            description: null,
            parsed: parsed
        }

        // remove leading slash
        const relative = pathName.substr(1 );
        let lastResolved : { fullPath?:string } = {};
        const resolve = foldersScan.find( ( mapper )=>{

            const fullPath = resolvePath( mapper.folder, relative );
            let withFullPath;
            if( mapper.with === "index" ){
                withFullPath = path.join( fullPath,  `${ mapper.with }${ mapper.type }`);
            } else {
                withFullPath =  `${fullPath}${ mapper.type }`;
            }

            const exists = fs.existsSync( withFullPath );

            if( !exists ) return false;
            const stat = fs.statSync( withFullPath );
            if( stat.isFile() ) return Object.assign( lastResolved, { fullPath: withFullPath, folder: mapper.folder } );

        });

        const extension = path.extname( relative );
        const filename = path.basename( relative );
        const name = path.basename( filename, extension );
        const fullDir = !!( resolve )? resolve.folder: undefined;
        const fullPath = !!( resolve )? path.join( resolve.folder, relative ): undefined;
        const relativeDir = path.dirname( relative );

        const description: PathDescription = { extension, filename, name, fullPath, fullDir,  relative,  relativeDir,
            folder: (resolve)? resolve.folder: undefined,
            resolve: (resolve)? lastResolved.fullPath: undefined,
            url: req.originalUrl,
            withRelative: (resolve)? path.join( relative, resolve.with ): relative,
            needSlash: dirSlash
                && ( relative.charAt( relative.length -1 ) !== "/" ) //no has slash
                && ( relative || "" ).length > 0  //not at the root
                && resolve?.with === "index"    //need concat with index
        };

        return {
            code: !resolve? 404: 200,
            message: !resolve? "404 request file not found": "file found",
            status: !!resolve,
            canNext: true,
            resolved: !!resolve? lastResolved.fullPath: null,
            mapper: resolve,
            description,
            parsed
        }
    }

    async function listen ( req: express.Request, res: Response, next: express.NextFunction ){
        const method = req.method.toUpperCase();
        if( method !== "GET" ) return next();

        const result = resolveRequest( req );
        const { canNext, status, message, resolved, code, mapper, description } = result;
        if( !status && !canNext ) return reject( res, { code, status, message } );
        else if( !status && canNext ) return next();

        //Redirect to page without extension with slash/
        if( description && description.needSlash ){
            return canAccept({ pageType: PageType.staticPage, description, data: null }, req, res,
                () => res.redirect( `/${description.relative}/`  )
            );
        }

        //Hidden index
        if( hiddenIndex && description && mapper && description.name === "index" && [ ".html", ".ejs" ].includes( mapper.type ) ){
            return canAccept({ pageType: PageType.staticPage, description, data: null }, req, res,
                () => res.redirect( `/${ description.relativeDir }${ dirSlash? "/":"" }` )
            );
        }

        //Redirect to page without extension
        if( mapper?.resolver === "static" && description?.extension === ".html" && description ){
            return canAccept({ pageType: PageType.staticPage, description, data: null }, req, res,
                () => res.redirect( `/${ path.join( description.relativeDir, description.name ) }` )
            );
        }

        //Send html file if exists
        if(  description && mapper?.resolver === "static" ){
            return canAccept({ pageType: PageType.staticPage, description, data: null }, req, res,
                () => res.sendFile( resolved )
            );
        }

        //Render view if exists
        if( mapper?.resolver === "dynamic" && !!description ){let data;
            if( folders.contents && fs.existsSync( path.join( folders.contents, `${ description.withRelative }.json` ) ) ) data = require(  path.join( folders.contents, `${ description.relative }.json` ) );
            else if( folders.contents && fs.existsSync( path.join( folders.contents, `${ description.withRelative }.json5` ) ) ) data = require(  path.join( folders.contents, `${ description.relative }.json5` ) );
            else if( folders.contents && fs.existsSync( path.join( folders.contents, `${ description.withRelative }.js` ) ) ) data = require(  path.join( folders.contents, `${ description.relative }.js` ) );

            if( isContentView( data ) ) data = await data.load( req, res, next );
            else if( data?.default && isContentView( data?.default ) ) data = await data?.default.load( req, res, next );
            if( !data ) data = {};
            data.project = { version };

            return canAccept({ pageType: PageType.dynamicPage, description, data }, req, res,
                () => res.render( description.withRelative, data ) );
        }

        //Stop if resolved by special static route
        if( resolvers.find( ( resolver )=> resolver( description, req, res ) ) ){
            return true;
        }

        //Next
        return next();

    }

    const canAccept =( args: AcceptorArgs, req: Request, res: Response, onAccept:()=>any )=>{
        const accept = acceptors.find( (acceptor)=> acceptor( args, req, res ) );
        if( accept && onAccept ) onAccept();
        if( accept ) return  true;
        res.status( 403 );
        res.json( { status: false, message: "Forbidden access to request page", code: 403 } );
        return false;
    }

    function reject( res: express.Response, args:{ code, message, status} ){
        res.status( args.code );
        res.json( args );
        return;
    }

    return { acceptors, resolvers, listen }
}
