class ArrayUtil  {
    static extendsMethods() {
        Array.prototype.deleteElement = function ( element ) {
            const index = this.indexOf( element );
            if ( index === -1 ) return false;
            return this.splice( index, 1 );
        };
        Array.prototype.deleteLastElement = function ( element ) {
            const index = this.lastIndexOf( element );
            if ( index === -1 ) return false;
            return this.splice( index, 1 );
        };
    }
}

module.exports = { ArrayUtil };

